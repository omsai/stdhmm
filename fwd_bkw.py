def fwd_bkw(observations, states, end_st, start_prob, trans_prob, emm_prob):
    """Forward-backward algorithm."""
    # Forward part of the algorithm
    fwd = []
    f_prev = None
    for i, observation in enumerate(observations):
        f_curr = {}
        for st in states:
            if i == 0:
                # base case for the forward part
                prev_f_sum = start_prob[st]
            else:
                prev_f_sum = sum(f_prev[k] * trans_prob[k][st] for k in states)

            f_curr[st] = emm_prob[st][observation] * prev_f_sum

        fwd.append(f_curr)
        f_prev = f_curr

    p_fwd = sum(f_curr[k] *
                trans_prob[k][end_st]
                for k in states)

    # Backward part of the algorithm
    bkw = []
    b_prev = None
    for i, observation_plus in enumerate(reversed(observations[1:] + (None,))):
        b_curr = {}
        for st in states:
            if i == 0:
                # base case for backward part
                b_curr[st] = trans_prob[st][end_st]
            else:
                b_curr[st] = sum(trans_prob[st][l] *
                                 emm_prob[l][observation_plus] *
                                 b_prev[l]
                                 for l in states)

        bkw.insert(0, b_curr)
        b_prev = b_curr

    p_bkw = sum(start_prob[state] *
                emm_prob[state][observations[0]] *
                b_curr[state]
                for state in states)

    # Merging the two parts
    posterior = []
    for i in range(len(observations)):
        posterior.append({st: fwd[i][st] * bkw[i][st] / p_fwd
                          for st in states})

    assert p_fwd == p_bkw
    return fwd, bkw, posterior


if __name__ == '__main__':
    obs = ('normal', 'cold', 'dizzy')
    states = ('Healthy', 'Fever')
    end_state = 'E'
    prob_start = {'Healthy': 0.6, 'Fever': 0.4}
    prob_trans = {
        'Healthy': {'Healthy': 0.69, 'Fever': 0.3, 'E': 0.01},
        'Fever':   {'Healthy': 0.4, 'Fever': 0.59, 'E': 0.01},
    }
    prob_emit = {
        'Healthy': {'normal': 0.5, 'cold': 0.4, 'dizzy': 0.1},
        'Fever':   {'normal': 0.1, 'cold': 0.3, 'dizzy': 0.6},
    }
    fwd_bkw(obs, states, end_state, prob_start, prob_trans, prob_emit)
