#ifndef TRAINING_H
#define TRAINING_H

#include <stddef.h>		/* size_t */

#include "model.h"		/* model_t */

/* Baum-Welch algorithm. */

typedef struct matrix_st matrix_t;
double baum_welch_single(matrix_t* fwd, matrix_t* bkw, int* obs, model_t* hmm,
			 size_t len);

#endif	/* TRAINING_H */
