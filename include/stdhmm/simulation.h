#ifndef SIMULATION_H
#define SIMULATION_H

/* Simulate from Hidden Markov model. */

#include <stddef.h>		/* size_t */

#include "model.h"		/* model_t */
#include "random.h"		/* prng_t */

void simulate(int* obs, int* states, model_t* hmm, size_t len, prng_t* gen);

#endif	/* SIMULATION_H */
