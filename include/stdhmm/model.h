#ifndef MODEL_H
#define MODEL_H

/** Hidden Markov model. */
typedef struct model_st model_t;
/** Hidden Markov model size type. */
typedef unsigned char model_n_t;
int model_init(model_t** hmm,
	       const double trans[],
	       const double emis[],
	       const double start[],
	       const model_n_t emis_tied[],
	       const model_n_t n_states,
	       const model_n_t n_obs);
void model_destroy(model_t** hmm);

model_n_t get_n_states(model_t* hmm);
model_n_t get_n_obs(model_t* hmm);

model_n_t get_emis_tied(model_t* hmm, model_n_t state);

double get_prob_start(model_t* hmm, model_n_t state);
double get_prob_trans(model_t* hmm, model_n_t from, model_n_t to);
double get_prob_emis(model_t* hmm, model_n_t state, model_n_t obs);

void set_prob_trans(model_t** hmm, model_n_t from, model_n_t to,
		    double log_prob);
void set_prob_emis(model_t** hmm, model_n_t state, model_n_t obs,
		   double log_prob);

model_n_t* get_emis_tied_all(model_t* hmm);

double* get_prob_start_all(model_t* hmm);
double* get_prob_trans_all(model_t* hmm, model_n_t from);
double* get_prob_emis_all(model_t* hmm, model_n_t state);

#endif	/* MODEL_H */
