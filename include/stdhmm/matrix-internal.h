#ifndef MATRIX_INTERNAL_H
#define MATRIX_INTERNAL_H

#include <stddef.h>		/* size_t */

/** Matrix for calculating forward and backward probabilities. */
typedef struct matrix_st {
  /** Number of observation columns. */
  size_t len;
  /** Number of rows. */
  size_t n_rows;
  /** Probabilities array of [len x n_rows]. */
  double** cells;
} matrix_t;

/** Initialize a matrix of probabilties.

    @param matrix Output object to allocate.
    @param n_rows Number of rows.
    @param len Number of observation columns.
*/
void matrix_init(matrix_t** matrix, size_t n_rows, size_t len);
void matrix_destroy(matrix_t** matrix);

#endif  /* MATRIX_INTERNAL_H */
