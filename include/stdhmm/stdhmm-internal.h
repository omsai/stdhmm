#ifndef STDHMM_INTERNAL_H
#define STDHMM_INTERNAL_H

/* Private API. */

#include "ptr-internal.h"
#include "trellis-internal.h"

#endif	/* STDHMM_INTERNAL_H */
