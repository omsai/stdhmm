#ifndef LOG_SUM_EXP_INTERNAL_H
#define LOG_SUM_EXP_INTERNAL_H

/* Recursive log_sum_exp algorithm. */

double log_sum_exp(double x, double y);

#endif	/* LOG_SUM_EXP_INTERNAL_H */
