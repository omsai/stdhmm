#ifndef DECODING_H
#define DECODING_H

#include <stddef.h>		/* size_t */

#include "model.h"		/* model_t */

/* Viterbi algorithm. */

void viterbi(int* ret, int* obs, model_t* hmm, size_t len);

#endif	/* DECODING_H */
