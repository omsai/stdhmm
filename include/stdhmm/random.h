#ifndef RANDOM_INTERNAL_H
#define RANDOM_INTERNAL_H

/* Simulate from Hidden Markov model. */

typedef struct prng_st prng_t;

void random_init(prng_t** generator, unsigned long seed);
void random_destroy(prng_t** generator);

unsigned long random_draw(prng_t** generator);
unsigned char random_sample(prng_t** p, double* log_probs, unsigned char len);

#endif	/* RANDOM_INTERNAL_H */
