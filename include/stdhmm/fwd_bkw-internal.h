#ifndef FWD_BKW_INTERNAL_H
#define FWD_BKW_INTERNAL_H

#include "matrix-internal.h"	/* matrix_t */
#include "model.h"		/* model_t */

void forward_fill_matrix(matrix_t* matrix, int* obs, model_t* hmm, size_t len);
void backward_fill_matrix(matrix_t* matrix, int* obs, model_t* hmm, size_t len);

#endif  /* FWD_BKW_INTERNAL_H */
