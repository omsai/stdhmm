#ifndef TRELLIS_INTERNAL_H
#define TRELLIS_INTERNAL_H

#include <stddef.h>		/* size_t */

#include "model.h"		/* model_t */

/** Viterbi trellis node propagated probability with previous node. */
typedef struct trellis_node_st {
  /** Index of node in the previous trellis column. */
  int prev;
  /** Cummulative probability value. */
  double log_prob;
} trellis_node_t;

/** Viterbi trellis of propagated probabilities with paths. */
typedef struct trellis_st {
  /** Number of observations to decode. */
  size_t len;
  /** Number of hidden states to decode. */
  model_n_t n_states;
  /** Viterbi trellis nodes of propagated probabilities with paths. */
  trellis_node_t** nodes;
} trellis_t;

void trellis_init(trellis_t** trellis, model_n_t n_states, size_t len);
void trellis_destroy(trellis_t** trellis);

void viterbi_fill_trellis(trellis_t* trellis, int* obs, model_t* hmm,
			  size_t len);
void viterbi_choose_path(int* ret, trellis_t* trellis, model_n_t n_states,
			 size_t len);

#endif /* TRELLIS_INTERNAL_H */
