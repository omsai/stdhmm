/* Alphabetize, unless there's a compelling reason. */
#include <assert.h>		/* assert */
#include <stdlib.h>		/* malloc, free */

#include "model.h"		/* model_t */
#include "decoding.h"		/* viterbi */

int
main() {
  enum { NORMAL, COLD, DIZZY, N_OBS };
  enum { HEALTHY, FEVER, N_STATES };
  const double trans[] = { 0.7, 0.3,
			   0.4, 0.6 };
  const double emis[] = { 0.5, 0.4, 0.1,
			  0.1, 0.3, 0.6 };
  const double start[] = { 0.6, 0.4 };
  model_t* model = NULL;
  int ret = model_init(&model, trans, emis, start, NULL, N_STATES, N_OBS);
  assert(model != NULL);
  assert(ret == 0);

  size_t len = 3;
  int* observations = malloc(sizeof(int[len]));
  assert(observations != NULL);
  int* hidden_states = malloc(sizeof(int[len]));
  assert(hidden_states != NULL);

  observations[0] = NORMAL;
  observations[1] = COLD;
  observations[2] = DIZZY;
  viterbi(hidden_states, observations, model, len);
  assert(hidden_states[0] == HEALTHY);
  assert(hidden_states[1] == HEALTHY);
  assert(hidden_states[2] == FEVER);

  free(hidden_states);
  free(observations);
  model_destroy(&model);

  return 0;
}
