/* Alphabetize, unless there's a compelling reason. */
#include <assert.h>		/* assert */
#include <stdlib.h>		/* malloc, free */

#include "fwd_bkw-internal.h"
#include "helper-training.h"
#include "matrix-internal.h"
#include "random.h"
#include "simulation.h"
#include "training.h"

int
main() {
  /* Implement tying emission states together to pass this test. */
  const int SKIP_TEST = 77;
  return SKIP_TEST;
  
  /* TSS HMM example from doi:10.1038/ng.3142 supp. fig. 2 */
  enum { NO_SIGNAL, ENRICHED, DEPLETED, N_OBS };
  enum { B, N1, N2, N3, P1, P2, P3, N_STATES };
  const double trans[] =
    {/* B     N1   N2   N3     P1   P2    P3 */
     0.99, 0.005, 0.0, 0.0, 0.005, 0.0, 0.00, /* B  */
     0.00, 0.000, 1.0, 0.0, 0.000, 0.0, 0.00, /* N1 */
     0.00, 0.000, 0.5, 0.5, 0.000, 0.0, 0.00, /* N2 */
     1.00, 0.000, 0.0, 0.0, 0.000, 0.0, 0.00, /* N3 */
     0.00, 0.000, 0.0, 0.0, 0.500, 0.5, 0.00, /* P1 */
     0.00, 0.000, 0.0, 0.0, 0.450, 0.1, 0.45, /* P2 */
     0.50, 0.000, 0.0, 0.0, 0.000, 0.0, 0.50  /* P3 */
    };
  const double emis[] =
    {/* ns enriched  depleted */
     0.90,   0.05,     0.05, /* B  */
     0.09,   0.90,     0.01, /* N1 */
     0.09,   0.90,     0.01, /* N2 */
     0.09,   0.90,     0.01, /* N3 */
     0.10,   0.45,     0.45, /* P1 */
     0.10,   0.45,     0.45, /* P2 */
     0.10,   0.45,     0.45, /* P3 */
    };
  const model_n_t emis_tied[] = {1, 2, 2, 2, 3, 3, 3};
  model_t* actual = NULL;
  const double start[] = { 0, 0.5, 0, 0, 0.5, 0, 0 };
  int ret = model_init(&actual, trans, emis, start, emis_tied, N_STATES, N_OBS);
  assert(actual != NULL);
  assert(ret == 0);

  model_t* expected = NULL;
  ret = model_init(&expected, trans, emis, start, emis_tied, N_STATES,
		   N_OBS);
  assert(expected != NULL);
  assert(ret == 0);

  /* Arguments to simulate. */
  prng_t* gen = NULL;
  random_init(&gen, 123);
  assert(gen != NULL);
  size_t len = 1000;
  int* obs = malloc(sizeof(int[len]));
  int* states = malloc(sizeof(int[len]));

  /* Arguments to Baum-Welch algorithm. */
  matrix_t* fwd = NULL;
  matrix_t* bkw = NULL;
  matrix_init(&fwd, N_STATES, len);
  matrix_init(&bkw, N_STATES, len);
  assert(fwd != NULL);
  assert(bkw != NULL);

  for (size_t i = 0; i < 1; ++i) {
    simulate(obs, states, expected, len, gen);
    baum_welch_single(fwd, bkw, obs, actual, len);
  }

  assert(print_and_check_model(actual, expected, 0.1));
  
  matrix_destroy(&fwd);
  matrix_destroy(&bkw);
  assert(fwd == NULL);
  assert(bkw == NULL);

  free(obs);
  free(states);
  random_destroy(&gen);
  assert(gen == NULL);

  model_destroy(&actual);
  model_destroy(&expected);
  assert(actual == NULL);
  assert(expected == NULL);

  return 0;
}
