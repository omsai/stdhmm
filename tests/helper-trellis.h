#ifndef HELPER_TRELLIS_H
#define HELPER_TRELLIS_H

#include "trellis-internal.h"

/* Internal API for tests. */

double trlp(trellis_t* trellis, size_t i, model_n_t state);
void print_trellis(trellis_t* trellis);

#endif	/* HELPER_TRELLIS_H */
