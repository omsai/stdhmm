/* Alphabetize, unless there's a compelling reason. */
#include <assert.h>		/* assert */
#include <math.h>		/* log */
#include <stdlib.h>		/* malloc, free */

#include "model.h"	/* model_t model_init model_destroy */

int
main() {
  model_t* model = NULL;
  /* Rain-Dry, Low-High atmospheric pressure Markov model:
     https://cse.buffalo.edu/~jcorso/t/CSE555/files/lecture_hmm.pdf */
  double trans[] = { 0.3, 0.7,
		     0.2, 0.8 };
  double emis[] = { 0.6, 0.4,
		    0.45, 0.55 };
  double start[] = { 0.35, 0.65 };

  int ret = model_init(&model, trans, emis, start, NULL, 2, 2);
  assert(model != NULL);
  assert(ret == 0);

  assert(get_n_states(model) == 2);

  assert(get_prob_trans(model, 0, 0) == log(0.3));
  assert(get_prob_trans(model, 0, 1) == log(0.7));
  assert(get_prob_trans(model, 1, 0) == log(0.2));
  assert(get_prob_trans(model, 1, 1) == log(0.8));

  assert(get_prob_emis(model, 0, 0) == log(0.6));
  assert(get_prob_emis(model, 0, 1) == log(0.4));
  assert(get_prob_emis(model, 1, 0) == log(0.45));
  assert(get_prob_emis(model, 1, 1) == log(0.55));

  assert(get_prob_start(model, 0) == log(0.35));
  assert(get_prob_start(model, 1) == log(0.65));

  assert(get_emis_tied(model, 0) == 1);
  assert(get_emis_tied(model, 1) == 2);

  model_destroy(&model);

  return 0;
}
