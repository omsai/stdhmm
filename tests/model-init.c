/* Alphabetize, unless there's a compelling reason. */
#include <assert.h>		/* assert */
#include <stdlib.h>		/* malloc, free */

#include "model.h"		/* model_t model_init model_destroy */

int
main()
{
  model_t* model = NULL;
  /* Rain-Dry, Low-High atmospheric pressure Markov model:
     https://cse.buffalo.edu/~jcorso/t/CSE555/files/lecture_hmm.pdf */
  double trans[] = { 0.3, 0.7,
		     0.2, 0.8 };
  double emis[] = { 0.6, 0.4,
		    0.4, 0.6 };
  double start[] = { 0.4, 0.6 };
  int ret = model_init(&model, trans, emis, start, NULL, 2, 2);
  assert(model != NULL);
  assert(ret == 0);
  model_destroy(&model);
  assert(model == NULL);

  /* Add errors to trans line */
  double orig = trans[1];
  trans[1] = 0.1;
  ret = model_init(&model, trans, emis, start, NULL, 2, 2);
  assert(model == NULL);
  assert(ret == 1);

  trans[1] = 0.8;
  ret = model_init(&model, trans, emis, start, NULL, 2, 2);
  assert(model == NULL);
  assert(ret == 1);
  trans[1] = orig;

  ret = model_init(&model, trans, emis, start, NULL, 2, 2);
  assert(model != NULL);
  assert(ret == 0);
  model_destroy(&model);
  assert(model == NULL);

  /* Add errors to emis line */
  orig = emis[2];
  emis[2] = 0.5;
  ret = model_init(&model, trans, emis, start, NULL, 2, 2);
  assert(model == NULL);
  assert(ret == 1);
  emis[2] = orig;

  ret = model_init(&model, trans, emis, start, NULL, 2, 2);
  assert(model != NULL);
  assert(ret == 0);
  model_destroy(&model);
  assert(model == NULL);

  /* Add errors to start line */
  orig = start[0];
  start[0] = 0.5;
  ret = model_init(&model, trans, emis, start, NULL, 2, 2);
  assert(model == NULL);
  assert(ret == 1);
  start[0] = orig;

  ret = model_init(&model, trans, emis, start, NULL, 2, 2);
  assert(model != NULL);
  assert(ret == 0);
  model_destroy(&model);
  assert(model == NULL);

  return 0;
}
