/* Alphabetize, unless there's a compelling reason. */
#include <assert.h>		/* assert */
#include <math.h>		/* fabs, exp */
#include <stdio.h>		/* fprintf */
#include <stdlib.h>		/* malloc, free */

#include "helper-training.h"
#include "training.h"
#include "matrix-internal.h"

int
main() {
  /* TSS HMM example from doi:10.1038/ng.3142 supp. fig. 2 */
  enum { NO_SIGNAL, ENRICHED, DEPLETED, N_OBS };
  enum { B, N1, N2, N3, P1, P2, P3, N_STATES };
  const double trans[] =
    {/* B   N1   N2   N3    P1   P2    P3 */
     0.9, 0.05, 0.0, 0.0, 0.05, 0.0, 0.00, /* B  */
     0.0, 0.00, 1.0, 0.0, 0.00, 0.0, 0.00, /* N1 */
     0.0, 0.00, 0.5, 0.5, 0.00, 0.0, 0.00, /* N2 */
     1.0, 0.00, 0.0, 0.0, 0.00, 0.0, 0.00, /* N3 */
     0.0, 0.00, 0.0, 0.0, 0.50, 0.5, 0.00, /* P1 */
     0.0, 0.00, 0.0, 0.0, 0.45, 0.1, 0.45, /* P2 */
     0.5, 0.00, 0.0, 0.0, 0.00, 0.0, 0.50  /* P3 */
    };
  const double emis[] =
    {/* ns enriched  depleted */
     0.80,   0.10,     0.10, /* B  */
     0.09,   0.70,     0.21, /* N1 */
     0.09,   0.70,     0.21, /* N2 */
     0.09,   0.70,     0.21, /* N3 */
     0.15,   0.40,     0.45, /* P1 */
     0.15,   0.40,     0.45, /* P2 */
     0.15,   0.40,     0.45, /* P3 */
    };
  model_t* actual = NULL;
  const double start[] = { 0, 0.5, 0, 0, 0.5, 0, 0 };
  int ret = model_init(&actual, trans, emis, start, NULL, N_STATES, N_OBS);
  assert(actual != NULL);
  assert(ret == 0);

  size_t len = 100;
  int* observations = malloc(sizeof(int[len]));
  assert(observations != NULL);
  for (int i = 0; i < 100; ++i)
    observations[i] = B;
  observations[0] = N1;
  observations[1] = N2;
  observations[2] = N2;
  observations[3] = N2;
  observations[4] = N3;
  observations[55] = P1;
  observations[56] = P2;
  observations[57] = P2;
  observations[58] = P3;
  observations[59] = P3;
  observations[60] = P3;
  observations[61] = P3;
  observations[68] = P1;
  observations[69] = P2;
  observations[70] = P3;
  observations[71] = P3;
  matrix_t* fwd = NULL;
  matrix_t* bkw = NULL;
  matrix_init(&fwd, N_STATES, len);
  matrix_init(&bkw, N_STATES, len);
  assert(fwd != NULL);
  assert(bkw != NULL);
  baum_welch_single(fwd, bkw, observations, actual, len);

  const double trans_expected[] =
    {/*  B       N1       N2       N3       P1       P2       P3 */
     0.95306, 0.03611, 0.00000, 0.00000, 0.01083, 0.00000, 0.00000, /* B  */
     0.00000, 0.00000, 1.00000, 0.00000, 0.00000, 0.00000, 0.00000, /* N1 */
     0.00000, 0.00000, 0.46222, 0.53778, 0.00000, 0.00000, 0.00000, /* N2 */
     1.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, /* N3 */
     0.00000, 0.00000, 0.00000, 0.00000, 0.36161, 0.63839, 0.00000, /* P1 */
     0.00000, 0.00000, 0.00000, 0.00000, 0.35044, 0.10542, 0.54414, /* P2 */
     0.64888, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.35112  /* P3 */
    };
  const double emis_expected[] =
    {/* no signal   enriched  depleted */
     0.8077587, 0.07935539, 0.1128859, /* B  */
     0.6418965, 0.21298840, 0.1451151, /* N1 */
     0.6445192, 0.09714843, 0.2583323, /* N2 */
     0.1377931, 0.63071492, 0.2314920, /* N3 */
     0.4844341, 0.33670724, 0.1788587, /* P1 */
     0.2275640, 0.39022943, 0.3822066, /* P2 */
     0.2570503, 0.25837234, 0.4845774  /* P3 */
    };
  model_t* expected = NULL;
  ret = model_init(&expected, trans_expected, emis_expected, start, NULL,
		   N_STATES, N_OBS);
  assert(expected != NULL);
  assert(ret == 0);
  assert(print_and_check_model(actual, expected, 1e-5));

  matrix_destroy(&fwd);
  matrix_destroy(&bkw);
  assert(fwd == NULL);
  assert(bkw == NULL);

  free(observations);
  model_destroy(&actual);
  model_destroy(&expected);

  return 0;
}
