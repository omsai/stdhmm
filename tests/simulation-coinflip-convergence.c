/* Alphabetize, unless there's a compelling reason. */
#include <assert.h>		/* assert */
#include <stdlib.h>		/* malloc, free */

#include "fwd_bkw-internal.h"
#include "helper-training.h"
#include "matrix-internal.h"
#include "random.h"
#include "simulation.h"
#include "training.h"

int
main() {
  /* Coin-flip example from doi:10.1038/s41592-019-0702-6 */
  enum { HEADS, TAILS, N_OBS };
  enum { FAIR, BIASED, N_STATES };
  const double trans[] = { 0.6, 0.4,
			   0.4, 0.6 };
  const double emis[] = { 0.5, 0.5,
			  0.3, 0.7 };
  const double start[] = { 0.5, 0.5 };
  model_t* actual = NULL;
  int ret = model_init(&actual, trans, emis, start, NULL, N_STATES, N_OBS);
  assert(actual != NULL);
  assert(ret == 0);

  const double trans_expected[] = { 0.8, 0.2,
				    0.2, 0.8 };
  const double emis_expected[] = { 0.5, 0.5,
				   0.1, 0.9 };
  model_t* expected = NULL;
  ret = model_init(&expected, trans_expected, emis_expected, start, NULL,
		   N_STATES, N_OBS);
  assert(expected != NULL);
  assert(ret == 0);

  /* Arguments to simulate. */
  prng_t* gen = NULL;
  random_init(&gen, 123);
  assert(gen != NULL);
  size_t len = 1000;
  int* obs = malloc(sizeof(int[len]));
  int* states = malloc(sizeof(int[len]));

  /* Arguments to Baum-Welch algorithm. */
  matrix_t* fwd = NULL;
  matrix_t* bkw = NULL;
  matrix_init(&fwd, N_STATES, len);
  matrix_init(&bkw, N_STATES, len);
  assert(fwd != NULL);
  assert(bkw != NULL);

  for (size_t i = 0; i < 30; ++i) {
    simulate(obs, states, expected, len, gen);
    baum_welch_single(fwd, bkw, obs, actual, len);
  }

  assert(print_and_check_model(actual, expected, 0.1));

  matrix_destroy(&fwd);
  matrix_destroy(&bkw);
  assert(fwd == NULL);
  assert(bkw == NULL);

  free(obs);
  free(states);
  random_destroy(&gen);
  assert(gen == NULL);

  model_destroy(&actual);
  model_destroy(&expected);
  assert(actual == NULL);
  assert(expected == NULL);

  return 0;
}
