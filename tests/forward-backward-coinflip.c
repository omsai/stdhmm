/* Alphabetize, unless there's a compelling reason. */
#include <assert.h>		/* assert */
#include <math.h>		/* fabs, exp */
#include <stdio.h>		/* fprintf */
#include <stdlib.h>		/* malloc, free */

#include "model.h"		/* model_t */
#include "fwd_bkw-internal.h"

int
main() {
  /* Coin-flip example from doi:10.1038/s41592-019-0702-6 */
  enum { HEADS, TAILS, N_OBS };
  enum { FAIR, BIASED, N_STATES };
  const double trans[] = { 0.6, 0.4,
			   0.4, 0.6 };
  const double emis[] = { 0.5, 0.5,
			  0.3, 0.7 };
  const double start[] = { 0.5, 0.5 };
  model_t* model = NULL;
  int ret = model_init(&model, trans, emis, start, NULL, N_STATES, N_OBS);
  assert(model != NULL);
  assert(ret == 0);

  size_t len = 5;
  int* observations = malloc(sizeof(int[len]));
  assert(observations != NULL);

  observations[0] = HEADS;
  observations[1] = HEADS;
  observations[2] = TAILS;
  observations[3] = TAILS;
  observations[4] = TAILS;
  matrix_t* matrix = NULL;
  matrix_init(&matrix, N_STATES, len);
  assert(matrix != NULL);
  forward_fill_matrix(matrix, observations, model, len);

  double expected_fwd[5][2] =
    {
     /* FAIR, BIASED */
     {-1.38629, -1.89712},
     {-2.25379, -2.86470},
     {-3.14888, -2.93107},
     {-3.74914, -3.36927},
     {-4.27268, -3.86110}
    };
  double expected_total =
    exp(expected_fwd[len-1][0]) +
    exp(expected_fwd[len-1][1]);
  double total =
    exp(matrix->cells[0][len-1]) +
    exp(matrix->cells[1][len-1]);

  /* Assertions don't print actual values. */
  fprintf(stderr, "Forward matrix states\n i   st     actual   expected\n");
  for (size_t i = 0; i < len; ++i)
    for (int state = FAIR; state < N_STATES; ++state)
      fprintf(stderr, "%2zu : %d -> %8.5f [%9.5f ]\n",
	      i, state,
	      matrix->cells[state][i],
	      expected_fwd[i][state]);

  const double tol = 1e-5;
  for (size_t i = 0; i < len; ++i)
    for (int state = FAIR; state < N_STATES; ++state)
      assert(fabs(matrix->cells[state][i] - expected_fwd[i][state]) < tol);

  fprintf(stderr, "Total  -> %8.5f [%9.5f ]\n",
	  total, expected_total);
  assert(fabs(total - expected_total) < tol);

  backward_fill_matrix(matrix, observations, model, len);

  double expected_bkw[5][2] =
    {
     /* FAIR, BIASED */
     {-2.405564, -2.490074},
     {-1.562211, -1.480130},
     {-1.056703, -0.977103},
     {-0.544727, -0.478036},
     { 0.000000,  0.000000},
    };
  /* We neglect to calculate the -1 column of the backward matrix, so
   * calculate it here for the total. */
  total = 0;
  total =
    exp(get_prob_start(model, 0) +
	get_prob_emis(model, 0, observations[0]) +
        matrix->cells[0][0]) +
    exp(get_prob_start(model, 1) +
	get_prob_emis(model, 1, observations[0]) +
	matrix->cells[1][0]);

  /* Assertions don't print actual values. */
  fprintf(stderr, "\nBackward matrix states\n i   st     actual   expected\n");
  for (size_t i = 0; i < len; ++i)
    for (int state = FAIR; state < N_STATES; ++state)
      fprintf(stderr, "%2zu : %d -> %8.5f [%9.5f ]\n",
	      i, state,
	      matrix->cells[state][i],
	      expected_bkw[i][state]);

  for (size_t i = 0; i < len; ++i)
    for (int state = FAIR; state < N_STATES; ++state)
      assert(fabs(matrix->cells[state][i] - expected_bkw[i][state]) < tol);

  fprintf(stderr, "Total  -> %8.5f [%9.5f ]\n",
	  total, expected_total);
  assert(fabs(total - expected_total) < tol);

  matrix_destroy(&matrix);
  assert(matrix == NULL);

  free(observations);
  model_destroy(&model);

  return 0;
}
