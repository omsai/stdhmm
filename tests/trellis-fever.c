/* Alphabetize, unless there's a compelling reason. */
#include <assert.h>		/* assert */
#include <math.h>		/* fabs */
#include <stdio.h>		/* fprintf */
#include <stdlib.h>		/* malloc, free */

#include "helper-trellis.h"
#include "decoding.h"		/* viterbi_fill_trellis */

int
main() {
  enum { NORMAL, COLD, DIZZY, N_OBS };
  enum { HEALTHY, FEVER, N_STATES };
  const double trans[] = { 0.7, 0.3,
			   0.4, 0.6 };
  const double emis[] = { 0.5, 0.4, 0.1,
			  0.1, 0.3, 0.6 };
  const double start[] = { 0.6, 0.4 };
  model_t* model = NULL;
  int ret = model_init(&model, trans, emis, start, NULL, N_STATES, N_OBS);
  assert(model != NULL);
  assert(ret == 0);

  size_t len = 3;
  int* observations = malloc(sizeof(int[len]));
  assert(observations != NULL);
  int* hidden_states = malloc(sizeof(int[len]));
  assert(hidden_states != NULL);

  observations[0] = NORMAL;
  observations[1] = COLD;
  observations[2] = DIZZY;
  trellis_t* trellis = NULL;
  trellis_init(&trellis, get_n_states(model), len);
  assert(trellis != NULL);

  viterbi_fill_trellis(trellis, observations, model, len);

  double expected[3][2] =
    {
     /* HEALTHY, FEVER */
     {-1.20397, -3.21888},
     {-2.47694, -3.61192},
     {-5.13620, -4.19174}
    };
  
  /* Assertions don't print actual values. */
  fprintf(stderr, "Trellis states\n i  obs     actual   expected\n");
  for (size_t i = 0; i < len; ++i)
    for (int state = HEALTHY; state < N_STATES; ++state)
      fprintf(stderr, "%2zu : %d -> %8.5f [%9.5f ]\n",
	      i, observations[i],
	      trlp(trellis, i, state),
	      expected[i][state]);
  
  const double tol = 1e-5;
  for (size_t i = 0; i < len; ++i)
    for (int state = HEALTHY; state < N_STATES; ++state)
      assert(fabs(trlp(trellis, i, state) - expected[i][state]) < tol);

  trellis_destroy(&trellis);
  assert(trellis == NULL);

  free(observations);
  free(hidden_states);
  model_destroy(&model);

  return 0;
}
