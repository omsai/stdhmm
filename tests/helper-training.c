/* Alphabetize, unless there's a compelling reason. */
#include <math.h>		/* fabs, exp */
#include <stdio.h>		/* fprintf */

#include "helper-training.h"

int
print_and_check_model(model_t* actual, model_t* expected, double tol)
{
  fprintf(stderr, "%-42s| Expected\n%42s|\n", "Trained", "");

  fprintf(stderr, "%-42s|\n", "Emission matrix");
  for (size_t ob = 0; ob < get_n_obs(actual); ++ob) {
    for (int state = 0; state < get_n_states(actual); ++state)
      fprintf(stderr, "%3.2f  ", exp(get_prob_emis(actual, state, ob)));
    fprintf(stderr, "| ");
    for (int state = 0; state < get_n_states(actual); ++state)
      fprintf(stderr, "%3.2f  ", exp(get_prob_emis(expected, state, ob)));
    fprintf(stderr, "\n");
  }

  fprintf(stderr, "%42s|\n%-42s|\n", "", "Transition matrix");
  for (size_t from = 0; from < get_n_states(actual); ++from) {
    for (int to = 0; to < get_n_states(actual); ++to) {
      double value = exp(get_prob_trans(actual, from, to));
      if (value != 0.0)
	fprintf(stderr, "%3.2f  ", value);
      else
	fprintf(stderr, "      ");
      }
    fprintf(stderr, "| ");
    for (int to = 0; to < get_n_states(actual); ++to) {
      double value = exp(get_prob_trans(expected, from, to));
      if (value != 0.0)
	fprintf(stderr, "%3.2f  ", value);
      else
	fprintf(stderr, "      ");
      }
    fprintf(stderr, "\n");
  }

  int ret = 1;
  const char fmt_header[] =
    " st   trained   expected     diff   <";
  const char fmt_row[] =
    "%2zu : %d -> %7.5f [%8.5f ] %8.5f  %s\n";
  fprintf(stderr, "\nEmission states\n %s %s %1.0e?\n", "ob", fmt_header, tol);
  for (size_t ob = 0; ob < get_n_obs(actual); ++ob)
    for (int state = 0; state < get_n_states(actual); ++state) {
      double diff =
	exp(get_prob_emis(actual, state, ob)) - 
	exp(get_prob_emis(expected, state, ob));
      int passed = fabs(diff) < tol;
      if (! passed) ret = 0;
      fprintf(stderr, fmt_row,
	      ob, state,
	      exp(get_prob_emis(actual, state, ob)),
	      exp(get_prob_emis(expected, state, ob)),
	      diff, passed ? "" : "NO");
    }

  fprintf(stderr, "\nTransition states\n %s %s %1.0e?\n", "st", fmt_header, tol);
  for (size_t from = 0; from < get_n_states(actual); ++from)
    for (int to = 0; to < get_n_states(actual); ++to) {
      double diff =
	exp(get_prob_trans(actual, from, to)) - 
	exp(get_prob_trans(expected, from, to));
      int passed = fabs(diff) < tol;
      if (! passed) ret = 0;
      fprintf(stderr, fmt_row,
	      from, to,
	      exp(get_prob_trans(actual, from, to)),
	      exp(get_prob_trans(expected, from, to)),
	      diff, passed ? "" : "NO");
    }

  return ret;
}
