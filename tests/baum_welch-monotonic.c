/* Alphabetize, unless there's a compelling reason. */
#include <assert.h>		/* assert */
#include <math.h>		/* fabs, exp */
#include <stdio.h>		/* fprintf */
#include <stdlib.h>		/* malloc, free */

#include "training.h"
#include "matrix-internal.h"

int
main() {
  /* Coin-flip example from doi:10.1038/s41592-019-0702-6 */
  enum { HEADS, TAILS, N_OBS };
  enum { FAIR, BIASED, N_STATES };
  const double trans[] = { 0.6, 0.4,
			   0.4, 0.6 };
  const double emis[] = { 0.5, 0.5,
			  0.3, 0.7 };
  const double start[] = { 0.5, 0.5 };
  model_t* model = NULL;
  int ret = model_init(&model, trans, emis, start, NULL, N_STATES, N_OBS);
  assert(model != NULL);
  assert(ret == 0);

  size_t len = 5;
  int* observations = malloc(sizeof(int[len]));
  assert(observations != NULL);

  observations[0] = HEADS;
  observations[1] = HEADS;
  observations[2] = TAILS;
  observations[3] = TAILS;
  observations[4] = TAILS;
  matrix_t* fwd = NULL;
  matrix_t* bkw = NULL;
  matrix_init(&fwd, N_STATES, len);
  matrix_init(&bkw, N_STATES, len);
  assert(fwd != NULL);
  assert(bkw != NULL);

  int l2_norm_is_monotonic = 1;
  int likelihood_is_monotonic = 1;
  fprintf(stderr, " i   l2-norm   <?  likelihood >?\n");
  double l2_norm_old = baum_welch_single(fwd, bkw, observations, model, len);
  double likelihood_old = fwd->cells[0][len-1];
  fprintf(stderr, " 0 : %f      %f\n", l2_norm_old, likelihood_old);
  for (int i = 1; i < 90; ++i) {
    double l2_norm_new = baum_welch_single(fwd, bkw, observations, model, len);
    double likelihood_new = fwd->cells[0][len-1];
    int is_lower_l2_norm = l2_norm_new <= l2_norm_old;
    int is_higher_likelihood = likelihood_new >= likelihood_old;
    fprintf(stderr, "%2d : %f  %s  %f  %s\n", i,
	    l2_norm_new, is_lower_l2_norm ? "  " : "NO",
	    likelihood_new, is_higher_likelihood ? "  " : "NO");
    if (! is_lower_l2_norm) l2_norm_is_monotonic = 0;
    if (! is_higher_likelihood) likelihood_is_monotonic = 0;
    l2_norm_old = l2_norm_new;
    likelihood_old = likelihood_new;
  }
  assert(l2_norm_is_monotonic);
  assert(likelihood_is_monotonic);

  matrix_destroy(&fwd);
  matrix_destroy(&bkw);
  assert(fwd == NULL);
  assert(bkw == NULL);

  free(observations);
  model_destroy(&model);

  return 0;
}
