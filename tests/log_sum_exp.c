/* Alphabetize, unless there's a compelling reason. */
#include <assert.h>		/* assert */
#include <math.h>		/* fabs, log, exp */
#include <stdio.h>		/* fprintf */

#include "log_sum_exp-internal.h"

void check(double x, double y, double expected)
{
  /* Assertions don't print actual values. */
  double result = log_sum_exp(x, y);
  fprintf(stderr, "log_sum_exp(%f, %f) = %f [ %f ]\n",
	  x, y, result, expected);
  const double tol = 1e-8;
  if (expected == 0.0 - INFINITY) {
    assert(expected == result);
  } else {
    assert(fabs(result - expected < tol));
  }
}

int
main() {
  check(log(0.1), log(0.1), log(0.2));
  check(log(0.0), log(1.0), log(1.0));
  check(log(1.0), log(0.0), log(1.0));
  check(log(0.0), log(0.0), log(0.0));

  return 0;
}
