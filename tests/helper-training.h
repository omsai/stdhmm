#ifndef HELPER_TRAINING_H
#define HELPER_TRAINING_H

#include "model.h"

/* Internal API for tests. */

int print_and_check_model(model_t* actual, model_t* expected, double tol);

#endif	/* HELPER_TRAINING_H */
