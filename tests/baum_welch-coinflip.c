/* Alphabetize, unless there's a compelling reason. */
#include <assert.h>		/* assert */
#include <math.h>		/* fabs, exp */
#include <stdio.h>		/* fprintf */
#include <stdlib.h>		/* malloc, free */

#include "helper-training.h"
#include "training.h"
#include "matrix-internal.h"

int
main() {
  /* Coin-flip example from doi:10.1038/s41592-019-0702-6 */
  enum { HEADS, TAILS, N_OBS };
  enum { FAIR, BIASED, N_STATES };
  const double trans[] = { 0.6, 0.4,
			   0.4, 0.6 };
  const double emis[] = { 0.5, 0.5,
			  0.3, 0.7 };
  const double start[] = { 0.5, 0.5 };
  model_t* trained = NULL;
  int ret = model_init(&trained, trans, emis, start, NULL, N_STATES, N_OBS);
  assert(trained != NULL);
  assert(ret == 0);

  size_t len = 5;
  int* observations = malloc(sizeof(int[len]));
  assert(observations != NULL);

  observations[0] = HEADS;
  observations[1] = HEADS;
  observations[2] = TAILS;
  observations[3] = TAILS;
  observations[4] = TAILS;
  matrix_t* fwd = NULL;
  matrix_t* bkw = NULL;
  matrix_init(&fwd, N_STATES, len);
  matrix_init(&bkw, N_STATES, len);
  assert(fwd != NULL);
  assert(bkw != NULL);
  baum_welch_single(fwd, bkw, observations, trained, len);

  const double emis_expected[] =
    {/* HEADS, TAILS */
     0.5118329, 0.4881671, /* FAIR */
     0.2891736, 0.7108264  /* BIASED */
    };
  const double trans_expected[] =
    {/* FAIR, BIASED */
     0.5633660, 0.4366340, /* FAIR */
     0.3490265, 0.6509735  /* BIASED */
    };
  model_t* expected = NULL;
  ret = model_init(&expected, trans_expected, emis_expected, start, NULL,
		   N_STATES, N_OBS);
  assert(expected != NULL);
  assert(ret == 0);
  assert(print_and_check_model(trained, expected, 1e-5));

  matrix_destroy(&fwd);
  matrix_destroy(&bkw);
  assert(fwd == NULL);
  assert(bkw == NULL);

  free(observations);
  model_destroy(&trained);
  model_destroy(&expected);

  return 0;
}
