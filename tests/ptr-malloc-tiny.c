#include <assert.h>		/* assert */
#include <stdlib.h>		/* malloc, free */

#include "ptr-internal.h"

int
main() {
  char* tiny = malloc(sizeof(char));
  check_ptr(tiny, "tiny");
  assert(tiny != NULL);
  free(tiny);

  return 0;
}
