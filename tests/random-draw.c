#include <assert.h>		/* assert */
#include <limits.h>		/* ULONG_MAX */
#include <math.h>		/* exp, fabs, log */
#include <stdio.h>		/* fprintf */
#include <stdlib.h>		/* NULL */

#include "random.h"	  /* random_init random_draw random_destroy */

int
main () {
  prng_t* gen = NULL;
  random_init(&gen, 123);
  assert(gen != NULL);

  /* Continuous uniform [0, 1] distribution. */
  size_t N = 100000;
  double sum = 0.0;
  for (size_t i = 0; i < N; ++i) {
    sum += random_draw(&gen) * 1.0 / ULONG_MAX;
  }
  double avg = sum / N;
  fprintf(stderr, "Average of %0.e draws from uniform [0, 1] = %f\n",
	  (double)N, avg);
  double tol = 1e-3;
  assert(fabs(avg - 0.5) < tol);

  /* Discrete uniform {0, 1, ..., 8} distribution. */
  N = 1000000;
  sum = 0.0;
  for (size_t i = 0; i < N; ++i) {
    sum += (random_draw(&gen) % 9) * 1.0;
  }
  avg = sum / N;
  fprintf(stderr, "Average of %0.e draws from {0, 1, ..., 8} = %f\n",
	  (double)N, avg);
  assert(fabs(avg - 4) < tol);

  /* Choosing values from a non-uniform array of probabilities. */
  N = 100000;
  double probs[2] = {log(0.3), log(0.7)};
  size_t counts[2] = {0, 0};
  unsigned char len = 2;
  for (size_t i = 0; i < N; ++i) {
    ++counts[random_sample(&gen, probs, len)];
  }
  double probs_actual[2] = {0.0, 0.0};
  for (unsigned char i = 0; i < len; ++i) {
    probs_actual[i] = counts[i] * 1.0 / N;
    fprintf(stderr, "%.1f biased draws of %ds = %f\n",
	    exp(probs[i]), i, probs_actual[i]);
  }
  for (unsigned char i = 0; i < len; ++i) {
    fprintf(stderr, "Checking %ds: %.1f == %f\n", i,
	    exp(probs[i]), probs_actual[i]);
    assert(fabs(exp(probs[i]) - probs_actual[i]) < tol);
  }

  random_destroy(&gen);
  assert(gen == NULL);
}
