#include <stdio.h>		/* fprintf */

#include "helper-trellis.h"

/* Internal API for tests. */

double
trlp(trellis_t* trellis, size_t i, model_n_t state)
{
  return trellis->nodes[i][state].log_prob;
}

void
print_trellis(trellis_t* trellis)
{
  fprintf(stderr, "Trellis populated with:\n i  ");
  for (model_n_t state = 0; state < trellis->n_states; ++state)
    fprintf(stderr, " %8d         ", state);
  fprintf(stderr, "\n");

  for (size_t i = 0; i < trellis->len; ++i) {
    fprintf(stderr, "%2zu  ", i);
    for (model_n_t state = 0; state < trellis->n_states; ++state)
      fprintf(stderr, "[%2d, %9.5f ]  ",
	      trellis->nodes[i][state].prev,
	      trellis->nodes[i][state].log_prob);
    fprintf(stderr, "\n");
  }
}
