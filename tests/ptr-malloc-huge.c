#include <assert.h>		/* assert */
#include <stdint.h>		/* SIZE_MAX */
#include <stdlib.h>		/* malloc */

#include "ptr-internal.h"

int
main() {
  char* too_big = malloc(SIZE_MAX / 2);
  check_ptr(too_big, "too_big");
}
