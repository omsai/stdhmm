/* Reuse the test case from the hmmviterbi function of the Octave Forge
   statistics package:
   https://sourceforge.net/p/octave/statistics/ci/default/tree/inst/hmmviterbi.m

   This highlights one of the limitations of our API; the observations and
   hidden states must be contiguous enumerations starting from 0! */
#include <assert.h>		/* assert */
#include <stdio.h>		/* fprintf */
#include <stdlib.h>		/* malloc */

#include "model.h"		/* model_t */
#include "decoding.h"		/* viterbi */

int
main() {
  const int N_STATES = 2;
  const int N_OBS = 3;
  const double trans[] = { 0.8, 0.2,
			   0.4, 0.6 };
  const double emis[] = { 0.2, 0.4, 0.4,
			  0.7, 0.2, 0.1 };
  const double start[] = { 1.0, 0.0 };
  model_t* model = NULL;
  int ret = model_init(&model, trans, emis, start, NULL, N_STATES, N_OBS);
  assert(model != NULL);
  assert(ret == 0);

  size_t len = 25;
  int* hidden_states = malloc(sizeof(int[len]));
  assert(hidden_states != NULL);

  int observations[] =
    { 0, 1, 0, 0, 0, 1, 1, 0, 1, 2, 2, 2, 2, 1, 2, 0, 0, 0, 0, 2, 2, 1, 2, 0,
      2 };
  viterbi(hidden_states, observations, model, len);

  int expected[] =
    { 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0,
      0 };

  /* Assertions don't print actual values. */
  fprintf(stderr, "Hidden states\n i  obs  actual expected\n");
  for (size_t i = 0; i < len; ++i)
    fprintf(stderr, "%2zu : %d -> %3d   [ %3d  ]\n",
	    i, observations[i],
	    hidden_states[i],
	    expected[i]);

  for (size_t i = 0; i < len; ++i)
    assert(hidden_states[i] == expected[i]);

  free(hidden_states);
  model_destroy(&model);

  return 0;
}
