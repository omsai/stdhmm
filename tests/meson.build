# ptr.h : check_ptr

test('check_ptr allows tiny malloc',
     executable(
       'ptr-malloc-tiny',
       'ptr-malloc-tiny.c',
       link_with : libstdhmm,
       include_directories : inc),
     suite : 'ptr')

test('check_ptr exits on unsatisfiable malloc',
     executable(
       'ptr-malloc-huge',
       'ptr-malloc-huge.c',
       link_with : libstdhmm,
       include_directories : inc),
     suite : 'ptr',
     should_fail : true)

# model.h : model_init model_destroy

test('model_init validates all probs',
     executable(
       'model-init',
       'model-init.c',
       link_with : libstdhmm,
       include_directories : inc),
     suite : 'model')

# model.h : get_n_states get_prob_trans get_prob_emis get_prob_start

test('model accessor API',
     executable(
       'model-accessors',
       'model-accessors.c',
       link_with : libstdhmm,
       include_directories : inc),
     suite : 'model')

# decoding.c : internal API

test('trellis calculates fever model probs',
     executable(
       'trellis-fever',
       ['trellis-fever.c',
	'helper-trellis.c'],
       link_with : libstdhmm,
       include_directories : inc),
     suite : 'decoding')

# test('trellis internal API headers are invisible')

# test('trellis internal API implementation is invisible')

# decoding.h : viterbi

test('viterbi decodes fever model',
     executable(
       'viterbi-fever',
       'viterbi-fever.c',
       link_with : libstdhmm,
       include_directories : inc),
     suite : 'decoding')

test('viterbi decodes tss model',
     executable(
       'viterbi-tss',
       ['viterbi-tss.c',
	'helper-trellis.c'],
       link_with : libstdhmm,
       dependencies : libm,
       include_directories : inc),
     suite : 'decoding')

test('viterbi decodes octave model',
     executable(
       'viterbi-octave',
       'viterbi-octave.c',
       link_with : libstdhmm,
       include_directories : inc),
     suite : 'decoding')

# training.h : internal API

test('log_sum_exp (LSE)',
     executable(
       'log_sum_exp',
       'log_sum_exp.c',
       dependencies : libm,	# exp()
       link_with : libstdhmm,
       include_directories : inc),
     suite : 'training')

# training.h : forward and backward algorithms

test('forward and backward algorithms for coin-flip model',
     executable(
       'forward-backward-coinflip',
       'forward-backward-coinflip.c',
       dependencies : libm,	# exp()
       link_with : libstdhmm,
       include_directories : inc),
     suite : 'training')

# training.h : Baum-Welch algorithm

test('baum-welch algorithm for coin-flip model',
     executable(
       'baum_welch-coinflip',
       ['baum_welch-coinflip.c',
	'helper-training.c'],
       dependencies : libm,	# exp()
       link_with : libstdhmm,
       include_directories : inc),
     suite : 'training')

test('baum-welch algorithm for TSS HMM model',
     executable(
       'baum_welch-tsshmm',
       ['baum_welch-tsshmm.c',
	'helper-training.c'],
       dependencies : libm,	# exp()
       link_with : libstdhmm,
       include_directories : inc),
     suite : 'training')

test('baum-welch algorithm is l2-norm monotonic',
     executable(
       'baum_welch-monotonic',
       'baum_welch-monotonic.c',
       link_with : libstdhmm,
       include_directories : inc),
    suite : 'training')

# simulation.h : internal API

test('random_draw generates uniform distribution',
     executable(
       'random-draw',
       'random-draw.c',
       dependencies : libm,	# exp()
       link_with : libstdhmm,
       include_directories : inc),
    suite : 'simulation')

test('coin-flip model converges to true parameters',
     executable(
       'simulation-coinflip-convergence',
       ['simulation-coinflip-convergence.c',
	'helper-training.c'],
       dependencies : libm,	# exp()
       link_with : libstdhmm,
       include_directories : inc),
    suite : 'simulation')

test('TSS HMM model parameters do not drift',
     executable(
       'simulation-tsshmm-stability',
       ['simulation-tsshmm-stability.c',
	'helper-training.c'],
       dependencies : libm,	# exp()
       link_with : libstdhmm,
       include_directories : inc),
    suite : 'simulation')

test('TSS HMM model converges to true parameters',
     executable(
       'simulation-tsshmm-convergence',
       ['simulation-tsshmm-convergence.c',
	'helper-training.c'],
       dependencies : libm,	# exp()
       link_with : libstdhmm,
       include_directories : inc),
    suite : 'simulation')
