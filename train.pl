#!/bin/env perl

=pod

=head1 NAME

script - description

=head1 SYNOPSIS

  ./script -help
  ./script -man
  ./script -debug

=head1 DESCRIPTION

=head1 OPTIONS

=cut

use strict;
use diagnostics;
use warnings FATAL=>"all";

use Hash::Merge;
use Carp;
use Config::General;
use Cwd qw(getcwd abs_path);
use Data::Dumper;
use File::Basename;
use FindBin;
use Getopt::Long;
use Math::Round qw(round nearest);
use Math::VecStat qw(sum min max average);
use Pod::Usage;
use Time::HiRes qw(gettimeofday tv_interval);
use Statistics::Basic qw(median stddev);
use Set::IntSpan;
use Storable;
use lib "$FindBin::RealBin";
use lib "$FindBin::RealBin/../lib";
use lib "$FindBin::RealBin/lib";
Hash::Merge::set_behavior( 'RIGHT_PRECEDENT' );

our (%OPT,%CONF,$conf);
our @COMMAND_LINE = ("file=s",
		     "configfile=s",
                     "scenario_name=s",
		     "help",
		     "cdump:s",
		     "man",
		     "debug");
our $VERSION = 0.01;
use File::Tee qw(tee);

$Data::Dumper::Indent    = 2;
$Data::Dumper::Quotekeys = 0;
$Data::Dumper::Terse     = 0;
$Data::Dumper::Sortkeys  = 1;

use Clone qw(clone);
use Math::Matrix;

# read and parse configuration file
parse_config();

sub validateconfiguration {

}

printdebug("scenarios: ",$CONF{scenario_name});

srand($CONF{seed}) if defined $CONF{seed};

my $truth    = init_matrix("truth");
my $estimate = $CONF{estimate_is_truth} ? init_matrix("truth") : init_matrix("estimate");

my $seq_len  = $CONF{seq_len};
my $seq_num  = $CONF{seq_num};

my $seq      = $CONF{seq} ? [ {e=>$CONF{seq}} ] : generate_sequence($seq_num,$seq_len,$truth);

#printdumperq($seq);
$seq_num = min($CONF{seq_num},int(@$seq));

$CONF{forks} = min($CONF{forks},int(@$seq));
printdebug("...forking",$CONF{forks});
for my $job (0..$CONF{forks}-1) {
    my $pid = fork();
    if($pid) {
	# in parent
    } else {
	tee STDOUT, ">", "out/train.$job.txt";
	for my $ensemble_size (Set::IntSpan->new($CONF{ensemble_size})->elements) {
	    next if $ensemble_size > @$seq;
	    next unless $ensemble_size % $CONF{forks} == $job;
	    my $ensemble = [@{$seq}[0..$ensemble_size-1]];
	    printinfo("ensemble P truth",likelihood_ensemble($ensemble,$truth));
	    printinfo("ensemble P estimate",likelihood_ensemble($ensemble,$estimate));
	    my $solution = em($ensemble,clone($estimate));
	    printinfo("ensemble P solution",likelihood_ensemble($ensemble,$solution));
	}
	exit;
    }
}

for (1 .. $CONF{forks}) {
    my $pid = wait();
}

exit;

################################################################

sub em {
    my $ensemble        = shift;
    my $estimate        = shift;
    my $estimate_prev;
    my $likelihood_prev = 0;
    my $likelihood      = 0;
    my $likelihood_best = 0;
    my $likelihood_d;
    my $iter_max;
    for my $iter (0..$CONF{iter_max}) {
	$likelihood_best = 0;
	$likelihood      = 0;
	$iter_max = $iter;
	if($iter == 0 ) {
	    $likelihood_best = likelihood_ensemble($ensemble,$estimate);
	} else {
	    # Estimate: alpha, beta
	    # Maximize: train()
	    my @estimates;
	    my @alpha;
	    my @beta;
	    for my $i (0..@$ensemble-1) {
		my $seq_this        = $ensemble->[$i];
		my $alpha           = alpha($seq_this,$estimate);
		my $beta            = beta($seq_this,$estimate);
		my $estimate_this   = estimate($estimate,$alpha,$beta,$seq_this);
		push @estimates, $estimate_this;
	    }
	    # Pick the best estimate - lowest likelihood
	    for my $estimate_this (@estimates) {
		my $likelihood_this = likelihood_ensemble($ensemble,$estimate_this);
		if(! $likelihood_best || $likelihood_this < $likelihood_best) {
		    $likelihood_best = $likelihood_this;
		    $estimate        = $estimate_this;
		}
	    }
	    #$estimate  = estimate_ensemble($estimate,\@alpha,\@beta,$ensemble);
	    #$estimate = combine(@estimates);
	}
	$likelihood_d    = ($likelihood_best - $likelihood_prev)/$likelihood_best;
	$likelihood_prev = $likelihood_best;
	if($CONF{report_each_iter}) {
	    printinfo(sprintf("EM flips %d t = %d mre E = %.3f T = %.3f P(%d|model) = %.3f (%.3f) d %.8f",
			      @$ensemble * $seq_len,
			      $iter_max,
			      maxre($truth->{E},$estimate->{E}),
			      maxre($truth->{T},$estimate->{T}),
			      @$ensemble * $seq_len,
			      $likelihood_best,
			      likelihood_ensemble($ensemble,$truth),
			      $likelihood_d));
	    report($estimate);
	}
	if($iter) {
	    last if abs($likelihood_d) < $CONF{likelihood_tol};
	    if($likelihood_d > 0) {
		# we've gone too far - P is dropped
		# take previous solution and exit
		$estimate = $estimate_prev;
		last;
	    }
	}
	$estimate_prev = $estimate;
    }
    if(! $CONF{report_each_iter}) {
	printinfo(sprintf("EM flips %d t = %d mre E = %.3f T = %.3f P(%d|model) = %.3f (%.3f) d %.8f",
			  @$ensemble * $seq_len,
			  $iter_max,
			  maxre($truth->{E},$estimate->{E}),
			  maxre($truth->{T},$estimate->{T}),
			  @$ensemble * $seq_len,
			  $likelihood_best,
			  likelihood_ensemble($ensemble,$truth),
			  $likelihood_d));
	report($estimate);
    }
    return $estimate;
}

################################################################

sub combine {
    my @matrices = @_;
    my $estimate;
    for my $type (qw(T E)) {
	for my $hs (qw(F B)) {
	    if($type eq "T") {
		for my $hs2 (qw( F B)) {
		    for my $m (@matrices) {
			$estimate->{$type}{$hs}{$hs2} += $m->{$type}{$hs}{$hs2} / @matrices;
		    }
		}
	    } else {
		for my $es (qw(T H)) {
		    for my $m (@matrices) {
			$estimate->{$type}{$hs}{$es} += $m->{$type}{$hs}{$es} / @matrices;
		    }
		}
	    }
	}
    }
    return $estimate;
}

sub maxre {
    my ($truth,$estimate) = @_;
    my $max = 0;
    for my $a (keys %$truth) {
	for my $b (keys %{$truth->{$a}}) {
	    my $d  = $estimate->{$a}{$b} - $truth->{$a}{$b};
	    my $re = abs($d)/$truth->{$a}{$b};
	    $max = $re if $re > $max;
	}
    }
    return $max;
}

sub report {
    my $matrix = shift;
    my @hs   = split(",",$CONF{elem}{rows});
    printinfo(sprintf("%5s %7s %7s",qw(E H T)));
    for my $hs (@hs) {
	printinfo(sprintf("%5s %7.5f %7.5f",
			  $hs,
			  $matrix->{E}{$hs}{H},
			  $matrix->{E}{$hs}{T}));
    }
    printinfo(sprintf("%5s %7s %7s",qw(T F B)));
    for my $hs (@hs) {
	printinfo(sprintf("%5s %7.5f %7.5f",
			  $hs,
			  $matrix->{T}{$hs}{F},
			  $matrix->{T}{$hs}{B}));
    }
}

sub likelihood_ensemble {
    my $seq    = shift;
    my $matrix = shift;
    my $likelihood = 0;
    for my $s (@$seq) {
	$likelihood += likelihood($s,$matrix);
    }
    return $likelihood;
}

sub likelihood {
    my $seq    = shift;
    my $matrix = shift;
    my $beta   = beta($seq,$matrix);
    my @hs   = split(",",$CONF{elem}{rows});
    my $cumul = 0;
    for my $hs (@hs) {
	$cumul += $beta->[0]{$hs};
    }
    return -log($cumul)/log(10);
}

sub estimate {
    my $matrix   = shift;
    my $alpha    = shift;
    my $beta     = shift;
    my $seq      = shift;
    my $estimate;# = clone($matrix);
    my @seqe = split("",$seq->{e});
    my @hs   = split(",",$CONF{elem}{rows});
    my $len  = @seqe;

    # E_BH
    my $num = 0;
    my $den = 0;
    for my $t (1..$len) {
	my $value = $alpha->[$t]{B} * $beta->[$t]{B};
	$num += $value if $seqe[$t-1] eq "H";
	$den += $value;
    }
    $estimate->{E}{B}{H} = $num/$den;
    $estimate->{E}{B}{T} = 1-$num/$den;

    # E_FH
    $num = 0;
    $den = 0;
    for my $t (1..$len) {
	my $value = $alpha->[$t]{F} * $beta->[$t]{F};
	$num += $value if $seqe[$t-1] eq "H";
	$den += $value;
    }
    if($CONF{fix}) {
	$estimate->{E}{F}{H} = 0.5 ; #$num/$den;
	$estimate->{E}{F}{T} = 0.5 ; #1-$num/$den;
    } else {
	$estimate->{E}{F}{H} = $num/$den;
	$estimate->{E}{F}{T} = 1-$num/$den;
    }

    # T_FF, T_FB
    for my $hs (@hs) {
	my $sum1 = 0;
	my $sum2 = 0;
	for my $t (1..$len-1) {
	    my $es_next = $seqe[$t];
	    my $v1 = $alpha->[$t]{$hs} * $matrix->{T}{$hs}{F} * $matrix->{E}{F}{$es_next} * $beta->[$t+1]{F};
	    my $v2 = $alpha->[$t]{$hs} * $matrix->{T}{$hs}{B} * $matrix->{E}{B}{$es_next} * $beta->[$t+1]{B};
	    #printinfo("T_FF",$hs,$t,$v1,$es_next);
	    $sum1 += $v1;
	    $sum2 += $v2;
	}
	#printinfo("T_FF",$sum1/($sum1+$sum2));
	my $e = $sum1/($sum1+$sum2);
	$estimate->{T}{$hs}{F} = $e;
	$estimate->{T}{$hs}{B} = 1-$e;
    }

    return $estimate;
}

sub estimate_ensemble {
    my $matrix    = shift;
    my $alpha     = shift;
    my $beta      = shift;
    my $ensemble  = shift;
    my $estimate;# = clone($matrix);

    my @hs   = split(",",$CONF{elem}{rows});

    # E_BH
    my $num = 0;
    my $den = 0;

    my $i = 0;
    for my $seq (@$ensemble) {
	my @seqe = split("",$seq->{e});
	my $len  = @seqe;
	my $b0   = $beta->[$i][0]{F} + $beta->[$i][0]{B};
	for my $t (1..$len) {
	    my $value = $alpha->[$i][$t]{B} * $beta->[$i][$t]{B} / $b0;
	    $num += $value if $seqe[$t-1] eq "H";
	    $den += $value;
	}
	$i++;
    }
    $estimate->{E}{B}{H} = $num/$den;
    $estimate->{E}{B}{T} = 1-$num/$den;

    # E_FH
    $num = 0;
    $den = 0;
    $i   = 0;
    for my $seq (@$ensemble) {
	my @seqe = split("",$seq->{e});
	my $len  = @seqe;
	my $b0   = $beta->[$i][0]{F} + $beta->[$i][0]{B};
	for my $t (1..$len) {
	    my $value = $alpha->[$i][$t]{F} * $beta->[$i][$t]{F} / $b0;
	    $num += $value if $seqe[$t-1] eq "H";
	    $den += $value;
	}
	$i++;
    }
    if($CONF{fix}) {
	$estimate->{E}{F}{H} = 0.5 ; #$num/$den;
	$estimate->{E}{F}{T} = 0.5 ; #1-$num/$den;
    } else {
	$estimate->{E}{F}{H} = $num/$den;
	$estimate->{E}{F}{T} = 1-$num/$den;
    }

    # T_FF, T_FB
    for my $hs (@hs) {
	my $sum1 = 0;
	my $sum2 = 0;
	$i = 0;
	for my $seq (@$ensemble) {
	    my @seqe = split("",$seq->{e});
	    my $len  = @seqe;
	    my $b0   = $beta->[$i][0]{F} + $beta->[$i][0]{B};
	    for my $t (1..$len-1) {
		my $es_next = $seqe[$t];
		my $v1 = $alpha->[$i][$t]{$hs} * $matrix->{T}{$hs}{F} * $matrix->{E}{F}{$es_next} *
		    $beta->[$i][$t+1]{F};
		my $v2 = $alpha->[$i][$t]{$hs} * $matrix->{T}{$hs}{B} * $matrix->{E}{B}{$es_next} *
		    $beta->[$i][$t+1]{B};
		#printinfo("T_FF",$hs,$t,$v1,$es_next);
		$sum1 += $v1 / $b0;
		$sum2 += $v2 / $b0;
	    }
	    $i++;
	}
	#printinfo("T_FF",$sum1/($sum1+$sum2));
	my $e = $sum1/($sum1+$sum2);
	$estimate->{T}{$hs}{F} = $e;
	$estimate->{T}{$hs}{B} = 1-$e;
    }
    return $estimate;
}

################################################################
# Forward probability, t = 1,2,3,...
#
# $alpha->[$t][HIDDEN_STATE]
sub alpha {
    my $seq    = shift;
    my $matrix = shift;
    my @hs     = split(",",$CONF{elem}{rows});
    my @es     = split(",",$CONF{elem}{cols});
    my @seqe   = split("",$seq->{e});
    my $len    = @seqe;
    my $alpha;
    for my $t (0..$len) {
	for my $hs (@hs) {
	    $alpha->[$t]{$hs} = 0;
	    if($t == 0) {
		$alpha->[$t]{$hs} = $CONF{stationary}{$hs};
	    } elsif ($t == 1) {
		my $es_this = $seqe[$t-1];
		$alpha->[$t]{$hs} = $alpha->[$t-1]{$hs} * $matrix->{E}{$hs}{$es_this};
	    } else {
		my $es_this = $seqe[$t-1];
		my $cumul = 0;
		for my $hs2 (@hs) {
		    $cumul += $alpha->[$t-1]{$hs2} * $matrix->{T}{$hs2}{$hs};
		}
		$alpha->[$t]{$hs} = $cumul * $matrix->{E}{$hs}{ $es_this };
	    }
	}
    }
    return $alpha;
}

################################################################
# Forward probability, t = 0,1,2,3,...,T
#
# $beta->[$t][HIDDEN_STATE]
sub beta {
    my $seq    = shift;
    my $matrix = shift;
    my @hs   = split(",",$CONF{elem}{rows});
    my @es   = split(",",$CONF{elem}{cols});
    my @seqe = split("",$seq->{e});
    my $len  = @seqe;
    my $beta;
    for my $t (reverse (0..$len)) {
	for my $hs (@hs) {
	    $beta->[$t]{$hs} = 0;
	    if($t == $len) {
		$beta->[$t]{$hs} = 1;
	    } elsif ($t > 0) {
		my $es_next = $seqe[$t];
		#printinfo("beta",$t,$es_next);
		my $cumul = 0;
		for my $hs2 (@hs) {
		    my $v = $matrix->{T}{$hs}{$hs2} * $matrix->{E}{$hs2}{$es_next} * $beta->[$t+1]{$hs2};
		    #printinfo("beta",$t,$hs,$hs2,$v);
		    $cumul += $v;
		}
		$beta->[$t]{$hs} = $cumul;
	    } elsif ($t == 0) {
		my $es_next = $seqe[$t];
		$beta->[$t]{$hs} = $CONF{stationary}{$hs} * $matrix->{E}{$hs}{$es_next} * $beta->[$t+1]{$hs};
	    }
	}
    }
    return $beta;
}

################################################################
# Generate a sequence of length $len.
#
# $seq->{e} emitted string
# $seq->{h} hidden string
sub generate_sequence {
    my $n      = shift;
    my $len    = shift;
    my $matrix = shift;
    # pick an initial state
    my @seq;
    my $table;
    my $norm;
    for my $j (1..$n) {
	my $hsprev = "";
	my $hs;
	my $es;
	my $seq;
	for my $i (1..$len) {
	    # hidden state
	    if(! $seq) {
		$hs = choose_item($CONF{stationary},rand());
	    } else {
		$hs = choose_item($matrix->{T}{$hs},rand());
	    }
	    #printinfo("hidden state",$hs);
	    # emitted state;
	    my $es = choose_item($matrix->{E}{$hs},rand());
	    $seq->{h} .= $hs;
	    $seq->{e} .= $es;
	    #printinfo("chain",$i,$hsprev . $hs, $hs.$es);
	    if($hsprev) {
		$table->{T}{$hsprev}{$hs} ++;
		$norm->{T}{$hsprev}++;
	    }
	    $table->{E}{$hs}{$es} ++;
	    $norm->{E}{$hs}++;
	    $hsprev = $hs;
	}
	push @seq, $seq;
    }
    $table->{T}{F}{F} /= $norm->{T}{F};
    $table->{T}{F}{B} /= $norm->{T}{F};
    $table->{T}{B}{F} /= $norm->{T}{B};
    $table->{T}{B}{B} /= $norm->{T}{B};
    $table->{E}{F}{T} /= $norm->{E}{F};
    $table->{E}{F}{H} /= $norm->{E}{F};
    $table->{E}{B}{T} /= $norm->{E}{B};
    $table->{E}{B}{H} /= $norm->{E}{B};
    report($table);
    return \@seq;
}

################################################################

# Given a hash of probabilities, choose a key from the hash
sub choose_item {
    my $hash  = shift;
    my $urd   = shift;
    my $p     = 0;
    for my $key (sort keys %$hash) {
	$p += $hash->{$key};
	if($urd < $p) {
	    return $key;
	}
    }
    printdumper($hash);
    die "Couldn't pick an item from hash";
}

sub init_matrix {
    my $type = shift;
    my $matrix;
    for my $name (keys %{$CONF{$type}}) {
	my $rows = [ split(",",$CONF{elem}{rows}) ];
	my $cols = [ split(",",$CONF{elem}{cols}) ];
	$cols = $rows if $name eq "T";
	my $i = 0;
	for my $value (split(",",$CONF{$type}{$name})) {
	    my $row = $i / 2;
	    my $col = $i % 2;
	    $matrix->{$name}{ $rows->[$row] }{ $cols->[$col] } = $value;
	    $i++;
	}
    }
    return $matrix;
}


sub read_file {
    my $file = shift;
    my $inputhandle = get_handle($file);
    my $data;
    while(<$inputhandle>) {
	next if /^\s*\#/;
	next if /^\s*$/g;
	chomp;
	my @tok = split;
	push @$data, \@tok;
    }
    return $data;
}

sub get_handle {
    my $file = shift;
    my $h;
    if($file) {
	die "No such file [$file]" unless -e $file;
	open(FILE,$file);
	$h = \*FILE;
    } else {
	$h = \*STDIN;
    }
    return $h;
}

# HOUSEKEEPING ###############################################################

{
    my $timers;
    my $latest;
    sub init_timer {
	my $name = shift;
	die "Must provide timer name" unless $name;
	$timers->{$name}{name} = $name;
	$timers->{$name}{on}   = 0;
	$timers->{$name}{n}    = 0;
	$timers->{$name}{dt}   = 0;
    }
    sub start_timer {
	my $name = shift || $latest;
	die "Must provide timer name" unless $name;
	init_timer($name);
	$timers->{$name}{on}   = 1;
	$timers->{$name}{t}    = [gettimeofday];
	$latest = $name;
    }
    sub stop_timer {
	my $name = shift || $latest;
	die "Must provide timer name" unless $name;
	if($timers->{$name}{on}) {
	    $timers->{$name}{on} = 0;
	    $timers->{$name}{n}++;
	    my $dt = tv_interval( $timers->{$name}{t} );
	    $timers->{$name}{dt} += $dt;
	}
    }
    sub report_timers {
	# turn off timers
	for my $timer (values %$timers) {
	    stop_timer($timer->{name}) if $timer->{on};
	}
	for my $timer (sort {$b->{dt} <=> $a->{dt}} values %$timers) {
	    printinfo(sprintf("timer %20s n %12d t %10.5f t/n %10.5f",
			      $timer->{name},
			      $timer->{n},
			      $timer->{dt},
			      $timer->{dt}/$timer->{n}));
	}
    }
}

sub dump_config {
    printdumper(\%OPT,\%CONF);
}

sub parse_config {
    GetOptions(\%OPT,@COMMAND_LINE);
    pod2usage() if $OPT{help};
    pod2usage(-verbose=>2) if $OPT{man};
    loadconfiguration($OPT{configfile});
    populateconfiguration();	# copy command line options to config hash
    validateconfiguration();
    if (defined $CONF{cdump}) {
	$Data::Dumper::Indent    = 2;
	$Data::Dumper::Quotekeys = 0;
	$Data::Dumper::Terse     = 0;
	$Data::Dumper::Sortkeys  = 1;
	$Data::Dumper::Varname = "OPT";
	printdumper(\%OPT);
	$Data::Dumper::Varname = "CONF";
	if($CONF{cdump}) {
	    my @path = split(",",$CONF{cdump});
	    my $node = $CONF{shift @path};
	    while(my $next = shift @path) {
		if($node->{$next}) {
		    $node = $node->{$next};
		} else {
		    last;
		}
	    }
	    printdumper($node);
	} else {
	    printdumper(\%CONF);
	}
	exit;
    }
}

sub populateconfiguration {
    for my $var (keys %OPT) {
	my $value = $OPT{$var};
	if($value =~ /^[+](.+)/ && $CONF{$var}) {
	    $CONF{$var} .= ",$1";
	} else {
	    $CONF{$var} = $OPT{$var};
	}
    }
    if($CONF{scenario_name}) {
	for my $name (split(",",$CONF{scenario_name})) {
	    if($CONF{scenario}{ $name }) {
		%CONF = %{ merge( \%CONF, $CONF{scenario}{ $name })}
	    }
	}
    }
    repopulateconfiguration(\%CONF);
}

sub repopulateconfiguration {
    my ($node,$parent_node_name) = shift;
    return unless ref($node) eq "HASH";
    for my $key (keys %$node) {
	my $value = $node->{$key};
	if (ref($value) eq "HASH") {
	    repopulateconfiguration($value,$key);
	} elsif (ref($value) eq "ARRAY") {
	    for my $item (@$value) {
		repopulateconfiguration($item,$key);
	    }
	} elsif (defined $value) {
	    my $new_value = parse_field($value,$key,$parent_node_name,$node);
	    $node->{$key} = $new_value;
	}
    }
}

sub parse_field {
    my ($str,$key,$parent_node_name,$node) = @_;
    # replace configuration field
    # conf(LEAF,LEAF,...)
    while ( $str =~ /(conf\(\s*(.+?)\s*\))/g ) {
	my ($template,$leaf) = ($1,$2);
	if (defined $template && defined $leaf) {
	    my @leaf         = split(/\s*,\s*/,$leaf);
	    my $new_template;
	    if (@leaf == 2 && $leaf[0] eq ".") {
		$new_template = $node->{$leaf[1]};
	    } else {
		$new_template = fetch_conf(@leaf);
	    }
	    $str =~ s/\Q$template\E/$new_template/g;
	}
    }
    if ($str =~ /\s*eval\s*\(\s*(.+)\s*\)/) {
	my $fn = $1;
	$str = eval $fn;
	if ($@) {
	    die "could not parse configuration parameter [$@]";
	}
    }
    return $str;
}

sub fetch_configuration {
    my @config_path = @_;
    my $node        = \%CONF;
    if(! @config_path) {
	return \%CONF;
    }
    for my $path_element (@config_path) {
	if (! exists $node->{$path_element}) {
	    return undef;
	} else {
	    $node = $node->{$path_element};
	}
    }
    return $node;
}

sub fetch_conf {
    return fetch_configuration(@_);
}

################################################################
#
#

sub loadconfiguration {
    my $file = shift;
    if (defined $file) {
	if (-e $file && -r _) {
	    # provided configuration file exists and can be read
	    $file = abs_path($file);
	} else {
	    confess "The configuration file [$file] passed with -configfile does not exist or cannot be read.";
	}
    } else {
	# otherwise, try to automatically find a configuration file
	my ($scriptname,$path,$suffix) = fileparse($0);
	my $cwd     = getcwd();
	my $bindir  = $FindBin::RealBin;
	my $userdir = $ENV{HOME} || ".";
	my @candidate_files = (
	    "$cwd/$scriptname.conf",
	    "$cwd/etc/$scriptname.conf",
	    "$cwd/../etc/$scriptname.conf",
	    "$bindir/$scriptname.conf",
	    "$bindir/etc/$scriptname.conf",
	    "$bindir/../etc/$scriptname.conf",
	    "$userdir/.$scriptname.conf",
	    );
	my @additional_files = ();
	for my $candidate_file (@additional_files,@candidate_files) {
	    #printinfo("configsearch",$candidate_file);
	    if (-e $candidate_file && -r _) {
		$file = $candidate_file;
		#printinfo("configfound",$candidate_file);
		last;
	    }
	}
    }
    if (defined $file) {
	$OPT{configfile} = $file;
	$conf = new Config::General(
	    -ConfigFile=>$file,
	    -IncludeRelative=>1,
	    -IncludeAgain=>1,
	    -ExtendedAccess=>1,
	    -AllowMultiOptions=>"yes",
	    -LowerCaseNames=>0,
	    -AutoTrue=>1
	    );
	%CONF = $conf->getall;
    }
}

sub printdebug {
    printerr(@_) if defined $CONF{debug};
}

sub printinfo {
    print join(" ",map { defined $_ ? $_ : "_undef_" } @_),"\n";
}

sub printfinfo {
    my ($fmt,@args) = @_;
    @args = map { defined $_ ? $_ : "_undef_" } @args;
    printf("$fmt\n",@args);
}

sub printerr {
    print STDERR join(" ",map { defined $_ ? $_ : "_undef_" } @_),"\n";
}

sub printdumper {
    print Dumper(@_);
}

sub printdumperq {
    printdumper(@_);
    exit;
}

sub printdumpertable {
    my $hash   = shift;
    my $depth  = shift || 0;
    my $fh     = shift || \*STDOUT;
    my $prefix = "#";
    print $fh ($prefix x 40)."\n" if ! $depth;
    my @keys = keys %$hash;
    my $maxlen = max( map { length($_) } @keys);
    for my $key (sort {ref($hash->{$a}) cmp ref($hash->{$b})} sort keys %$hash) {
	#printinfo($key);
	my $value  = defined $hash->{$key} ? $hash->{$key} : "UNDEF";
	my $fmtd   = sprintf("%%%dd",$depth+1);
	my $fmtdd  = sprintf("%%%dd",$depth+2);
	my $fmtddd = sprintf("%%%dd",$depth+3);
	my $fmtr   = sprintf("%%%ds",$maxlen);
	my $fmtl   = sprintf("%%-%ds",$maxlen);
	#printinfo(sprintf("# $fmtd #",1));
	#printinfo(sprintf("# $fmtdd #",1));
	if(ref $value eq "HASH") {
	    printf $fh ("# $fmtd * %s $fmtl\n",
			$depth,
			"  " x $depth,
			$key);
	    printdumpertable($value,$depth+1,$fh);
	} else {
	    printf $fh ("# $fmtd . %s $fmtr %s\n",
			$depth,
			"  " x $depth,
			$key,$value);
	}
    }
    print $fh ($prefix x 40)."\n" if ! $depth;
}

=pod

=head1 HISTORY

=over

=item * 27 Jun 2018

Added some modules and printdumperq.

=item * 24 Jun 2014

Fixed config dump.

=item * 12 May 2014

Added printfinfo().

=back

=head1 AUTHOR

Martin Krzywinski

=head1 CONTACT

Martin Krzywinski
Genome Sciences Center
BC Cancer Research Center
100-570 W 7th Ave
Vancouver BC V5Z 4S6

mkweb.bcgsc.ca
martink@bcgsc.ca

=cut
