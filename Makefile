CFLAGS = -g3 -O3 -fPIC -Wall -march=native -DSILENT -I./include/stdhmm
LDFLAGS = -shared -lm

SHARED_OBJECT = libstdhmm.so
OBJECTS = src/decoding.o src/model.o src/ptr-internal.o src/trellis-internal.o
ARTIFACTS = $(OBJECTS) $(SHARED_OBJECT)

$(SHARED_OBJECT) : $(OBJECTS)
	$(CC) $(LDFLAGS) -o $@ $^

.PHONY : clean
clean :
	rm -f $(ARTIFACTS)
