"""https://en.wikipedia.org/wiki/Viterbi_algorithm"""

from math import inf, log


def viterbi(obs, states, start_p, trans_p, emit_p):  # noqa E501 pylint: disable=redefined-outer-name 
    """Finding the most likely sequence of hidden states."""
    table = [{}]
    for state in states:
        table[0][state] = {"prob": start_p[state] + emit_p[state][obs[0]],
                           "prev": None}
    # Run Viterbi when i > 0.
    for i in range(1, len(obs)):
        table.append({})
        for state in states:
            max_tr_prob = (table[i - 1][states[0]]["prob"] +
                           trans_p[states[0]][state])
            prev_st_selected = states[0]
            for prev_st in states[1:]:
                tr_prob = (table[i - 1][prev_st]["prob"] +
                           trans_p[prev_st][state])
                if tr_prob > max_tr_prob:
                    max_tr_prob = tr_prob
                    prev_st_selected = prev_st
            max_prob = max_tr_prob + emit_p[state][obs[i]]
            table[i][state] = {"prob": max_prob,
                               "prev": prev_st_selected}
    for line in dptable(table):
        print(line)
    opt = []
    max_prob = -inf
    previous = None
    # Get most probable state and its backtrack.
    for state, data in table[-1].items():
        if data["prob"] > max_prob:
            max_prob = data["prob"]
            best_st = state
    opt.append(best_st)
    previous = best_st
    # Follow the backtrack till the first observation.
    for i in range(len(table) - 2, -1, -1):
        opt.insert(0, table[i + 1][previous]["prev"])
        previous = table[i + 1][previous]["prev"]
    print(f"The hidden states are {', '.join(opt)}")


def dptable(table):
    """Print a table of steps from dictionary."""
    yield " " * 9 + " ".join(("%7d" % i) for i in range(len(table)))
    for state in table[0]:
        yield "%7s: " % state + " ".join("%.5f" % v[state]["prob"]
                                         for v in table)


if __name__ == "__main__":
    obs = ("normal", "cold", "dizzy")
    states = ("healthy", "fever")
    prob_start = {"healthy": log(0.6), "fever": log(0.4)}
    prob_trans = {
        "healthy": {"healthy": log(0.7), "fever": log(0.3)},
        "fever": {"healthy": log(0.4), "fever": log(0.6)},
    }
    prob_emit = {
        "healthy": {"normal": log(0.5), "cold": log(0.4), "dizzy": log(0.1)},
        "fever": {"normal": log(0.1), "cold": log(0.3), "dizzy": log(0.6)},
    }
    viterbi(obs, states, prob_start, prob_trans, prob_emit)
