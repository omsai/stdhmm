# Standard HMM training and decoding

Implements HMMs with 2 non-standard features:

1. tying HMM emissions together, and
2. preserving the topology when training transition and emission probabilities.

Not supporting point 2 should be an implementation bug, but surprisingly is
missing from some R packages.

## About HMMs

HMMs are classically used 3 for purposes; to find:

1. the probability of an observed sequence by summing over all possible paths
   of hidden states (forward algorithm or backward algorithm),
2. the most likely path of hidden states for an observed sequence (Viterbi
   algorithm, see Wikipedia), and
3. estimating the transition and emission probabilities given an observed
   sequence and topology (Baum-Welch algorithm).

Whereas the Viterbi algorithm is well explained in the Wikipedia article, the
intuition behind the forward, backward and Baum-Welch algorithms are better
explained in the Nature article by Grewal, Krzywinski and Altman 2020
[doi:10.1038/ng.3142](http://www.nature.com/doifinder/10.1038/ng.3142a).

## High-level implementations in python from Wikipedia

- [viterbi.py](viterbi.py) : Wikipedia's Viterbi algorithm.
- [fwd_bkw.py](fwd_bkw.py) : Wikipedia's forward-backward algorithm.

## High-level implementation of the Nature article

[Nature Article](http://www.nature.com/doifinder/10.1038/ng.3142a)

- [training.R](training.R) : Baum-Welch from the paper.
- [training_pkg.R](training.R) : Baum-Welch
  using the CRAN HMM package.
- [train.pl](train.pl) : Martin Krzywinski's perl script which can be run with
  `./train.pl -configfile=train.conf`
- [hmm_simulations_nonpseudocounted.Rmd](hmm_simulations_nonpseudocounted.Rmd)
  Jasleen Grewal's R Markdown notebook.

## Low-level implementation in C

C was chosen over C++ for compatibility with R and Bioconductor; Bioconductor
tends not to favor C++.  An example `Makefile` is provided because the
automated quality checker for R's C extensions prefers vanilla, non-GNU, `make`
build system.

Generate `libstdhmm.so` using `make`:

```console
$ make
cc -g3 -O3 -fPIC -Wall -march=native -DSILENT   -c -o model.o model.c
cc -g3 -O3 -fPIC -Wall -march=native -DSILENT   -c -o ptr.o ptr.c
cc -g3 -O3 -fPIC -Wall -march=native -DSILENT   -c -o viterbi.o viterbi.c
cc -shared -lm -o libstdhmm.so model.o ptr.o viterbi.o
```

## Development with Meson

The `Makefile` is only provided as a template for builds for `R`, but otherwise
`meson` should be used for building, running `valgrind`, the unit tests, and
`gcovr` test coverage.

You can install `meson`, `ninja`, and `gcovr` via pip:

```console
python3 -m pip install --user --upgrade meson ninja gcovr
```

Contributors should follow GNU C coding standards
https://www.gnu.org/prep/standards/

### Create the build directory

In general, you would run all of the `meson` and `ninja` commands
inside `builddir/` after you create `builddir/` with `meson`:

```console
$ meson builddir
$ cd builddir/
```

### Build programs

```console
$ ninja
```

### Build documentation

```console
$ ninja doc
```

### Run tests

```console
$ meson test --print-errorlogs
```

### Inspect test coverage

Make sure you have the `gcovr` package installed,
then recreate `builddir/` with the `b_coverage=true` built-in build option:

```console
$ cd ..
$ rm -rf builddir/
$ meson -Db_coverage=true builddir
$ cd builddir/
$ meson test
$ ninja coverage-text
$ cat meson-logs/coverage.txt
```

### Check for memory leaks

```console
$ valgrind="valgrind --leak-check=full --show-leak-kinds=all --error-exitcode=1"
$ meson test --wrap="$valgrind"
```

### Debug with GDB

This debugging session is illustrative,
and is not attempting to fix any genuine problem:

```console
$ meson test --gdb "viterbi decodes fever model" # name of test

(gdb) break main
Breakpoint 1 at 0x1322: file ../tests/viterbi-fever.c, line 13.

(gdb) run
Starting program: /home/omsai/src/viterbi/builddir/tests/vtierbi-fever 

Breakpoint 1, main () at ../tests/viterbi-fever.c:13
13	  const double trans[] = { log1p(0.7), log1p(0.3),

(gdb) info locals
trans = {9.8813129168249309e-324, 4.6355705390048694e-310, 0, 0}
emis = {0, 7.7936064160557639e-317, 9.584873529320183e-322, 0, 6.9533558072690027e-310, 4.6355705385709316e-310}
start = {7.9050503334599447e-323, 6.9533491724637668e-310}
model = 0xa0
ret = 0
__PRETTY_FUNCTION__ = "main"
len = 140737488344160
observations = 0x555555555220 <_start>
hidden_states = 0x555555557880 <__libc_csu_init>
(gdb) watch hidden_states
Hardware watchpoint 2: hidden_states

(gdb) c
Continuing.

Hardware watchpoint 2: hidden_states

Old value = (int *) 0x555555557880 <__libc_csu_init>
New value = (int *) 0x55555555c390
main () at ../tests/viterbi-fever.c:27
27	  assert(hidden_states != NULL);

(gdb) p hidden_states@N_STATES
$3 = {0x55555555c390, 0x55555555c370}

(gdb) l
22	
23	  size_t len = 3;
24	  int* observations = malloc(sizeof(int[len]));
25	  assert(observations != NULL);
26	  int* hidden_states = malloc(sizeof(int[len]));
27	  assert(hidden_states != NULL);
28	
29	  observations[0] = NORMAL;
30	  observations[1] = COLD;
31	  observations[2] = DIZZY;
```
