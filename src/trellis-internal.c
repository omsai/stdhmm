#include <stdlib.h>		/* malloc, free, exit */

#include "ptr-internal.h"	/* check_ptr */
#include "trellis-internal.h"

void
trellis_init(trellis_t** trellis, model_n_t n_states, size_t len)
{
  *trellis = malloc(sizeof(trellis_t));
  check_ptr(trellis, "trellis");

  (*trellis)->len = len;
  (*trellis)->n_states = n_states;

  (*trellis)->nodes = malloc(sizeof(trellis_node_t*) * len);
  check_ptr((*trellis)->nodes, "nodes");
  for (size_t i = 0; i < len; ++i) {
    (*trellis)->nodes[i] = malloc(sizeof(trellis_node_t) * n_states);
    check_ptr((*trellis)->nodes[i], "nodes");
  }
}

void
trellis_destroy(trellis_t** trellis)
{
  for (size_t i = 0; i < (*trellis)->len; ++i) {
    free((*trellis)->nodes[i]);
    (*trellis)->nodes[i] = NULL;
  }
  free((*trellis)->nodes);
  (*trellis)->nodes = NULL;

  free((*trellis));
  *trellis = NULL;
}

