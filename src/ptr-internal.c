/* Alphabetize, unless there's a compelling reason. */
#include <errno.h>		/* errno */
#include <stdio.h>		/* printf */
#include <stdlib.h>		/* exit */
#include <string.h>		/* strerrno */

#include "ptr-internal.h"

void
check_ptr(void* ptr, const char* name) {
  if (ptr == NULL) {
#ifndef HIDE_ERRORS
    int err = errno;
    printf("Error  : Failed to allocate memory for %s\nReason : %s\n",
	   name, strerror(err));
#endif	/* HIDE_ERRORS */
    exit(EXIT_FAILURE);
  }
}
