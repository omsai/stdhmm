#include <math.h>		/* fabs, fmax, exp, log1p */

#include "log_sum_exp-internal.h"

/* See recursive implementation in https://rpubs.com/FJRubio/LSE */
double
log_sum_exp(double x, double y)
{
  /* Avoid nan from subtracting infinities.  In the formula below inside fabs()
     there is "x - y", but if one inputs log(0) = -inf to both x and y, that
     creates nan because "-inf - (-inf) = -inf + inf = inf - inf = nan". */
  if (x == 0.0 - INFINITY &&
      y == 0.0 - INFINITY) {
    /* log(0 + 0) == log(0) */
    return 0.0 - INFINITY;
  }
  return fmax(x, y) + log1p(exp(-fabs(x - y)));
}
