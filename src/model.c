/* Alphabetize, unless there's a compelling reason. */
#include <math.h>		/* log, exp, fabs */
#include <stdio.h>		/* fprintf */
#include <stdint.h>		/* uintptr_t */
#include <stdlib.h>		/* malloc */

#include "model.h"
#include "ptr-internal.h"

/** Hidden Markov model. */
struct model_st {
  /** Number of HMM discrete states. */
  model_n_t n_states;
  /** Number of HMM discrete emissions. */
  model_n_t n_obs;
  /* Don't use multi-dimensional arrays; instead dynamically allocate as
     advised by Kelly and Pohl's "A Book on C" 4th edition in chapter 12,
     section 6, on page 571. */
  /** Probability array of state transitions. */
  double** trans;
  /** Probability array of state emissions. */
  double** emis;
  /** Probability array of starting at particular states. */
  double* start;
  /** Array of tied emissions indices. */
  model_n_t* emis_tied;
};

/** Ensure probability rows sum to 1. */
int
valid_probs(const double trans[],
	    const double emis[],
	    const double start[],
	    model_n_t n_states,
	    model_n_t n_obs)
{
  const double tol = 1e-7;
  int is_valid = 1;

  for (model_n_t from = 0; from < n_states; ++from) {
    double sum = 0;
    for (model_n_t to = 0; to < n_states; ++to)
      sum += trans[from * n_states + to];
    if (fabs(sum - 1) > tol) {
      is_valid = 0;
      fprintf(stderr, "Error: Transition probabilities for "
	      "state %d sum to %.7f instead of 1!\n", from, sum);
    }
  }

  for (model_n_t state = 0; state < n_states; ++state) {
    double sum = 0;
    for (model_n_t obs = 0; obs < n_obs; ++obs)
      sum += emis[state * n_obs + obs];
    if (fabs(sum - 1) > tol) {
      is_valid = 0;
      fprintf(stderr, "Error: Emission probabilities for "
	      "state %d sum to %.7f instead of 1!\n", state, sum);
    }
  }

  double sum = 0;
  for (model_n_t state = 0; state < n_states; ++state) {
    sum += start[state];
  }
  if (fabs(sum - 1) > tol) {
    is_valid = 0;
    fprintf(stderr,
	    "Error: Starting probabilities sum to %.7f instead of 1!\n", sum);
  }

  return is_valid;
}

int
model_init(model_t** hmm,
	   const double trans[],
	   const double emis[],
	   const double start[],
	   const model_n_t emis_tied[],
	   const model_n_t n_states,
	   const model_n_t n_obs)
{
  if (! valid_probs(trans, emis, start, n_states, n_obs))
    return 1;

  *hmm = malloc(sizeof(model_t));
  check_ptr(hmm, "hmm");

  (*hmm)->n_states = n_states;
  (*hmm)->n_obs = n_obs;

  /* Log transform probabilities to maintain numeric precision during long
     series of multiplications.  And, yes, it turns out that taking log(0.0)
     does not adversely affect the calculations from inspecting the trellis;
     running viterbi using log1p() instead produces invalid results. */

  (*hmm)->trans = malloc(sizeof(double*) * n_states);
  check_ptr((*hmm)->trans, "trans");
  for (model_n_t from = 0; from < n_states; ++from) {
    (*hmm)->trans[from] = malloc(sizeof(double) * n_states);
    check_ptr((*hmm)->trans[from], "trans");
    for (model_n_t to = 0; to < n_states; ++to)
      (*hmm)->trans[from][to] = log(trans[from * n_states + to]);
  }

  (*hmm)->emis = malloc(sizeof(double*) * n_states);
  check_ptr((*hmm)->emis, "emis");
  for (model_n_t state = 0; state < (*hmm)->n_states; ++state) {
    (*hmm)->emis[state] = malloc(sizeof(double) * n_obs);
    check_ptr((*hmm)->emis[state], "emis");
    for (model_n_t obs = 0; obs < (*hmm)->n_obs; ++obs)
      (*hmm)->emis[state][obs] = log(emis[state * n_obs + obs]);
  }

  (*hmm)->start = malloc(sizeof(double) * n_states);
  check_ptr((*hmm)->start, "start");
  for (model_n_t state = 0; state < (*hmm)->n_states; ++state)
    (*hmm)->start[state] = log(start[state]);

  (*hmm)->emis_tied = malloc(sizeof(model_n_t) * n_obs);
  check_ptr((*hmm)->emis_tied, "emis_tied");
  for (model_n_t obs = 0; obs < (*hmm)->n_obs; ++obs)
    (*hmm)->emis_tied[obs] = emis_tied == NULL ? obs+1 : emis_tied[obs];

  return 0;
}

void
model_destroy(model_t** hmm)
{
  for (model_n_t state = 0; state < (*hmm)->n_states; ++state) {
    free((*hmm)->trans[state]);
    free((*hmm)->emis[state]);

    (*hmm)->trans[state] = NULL;
    (*hmm)->emis[state] = NULL;
  }
  free((*hmm)->trans);
  free((*hmm)->emis);
  free((*hmm)->start);
  if ((*hmm)->emis_tied != NULL)
    free((*hmm)->emis_tied);

  (*hmm)->trans = NULL;
  (*hmm)->emis = NULL;
  (*hmm)->start = NULL;
  (*hmm)->emis_tied = NULL;

  free(*hmm);
  *hmm = NULL;
}

model_n_t
get_n_states(model_t* hmm)
{
  return hmm->n_states;
}

model_n_t
get_n_obs(model_t* hmm)
{
  return hmm->n_obs;
}

model_n_t
get_emis_tied(model_t* hmm, model_n_t state)
{
  if (hmm->emis_tied == NULL)
    /* Double cast per https://stackoverflow.com/a/14711516 */
    return (model_n_t)(uintptr_t)NULL;
  return hmm->emis_tied[state];
}

double
get_prob_start(model_t* hmm, model_n_t state)
{
  return hmm->start[state];
}

double
get_prob_trans(model_t* hmm, model_n_t from, model_n_t to)
{
  return hmm->trans[from][to];
}

double
get_prob_emis(model_t* hmm, model_n_t state, model_n_t obs)
{
  return hmm->emis[state][obs];
}

void
set_prob_trans(model_t** hmm, model_n_t from, model_n_t to, double log_prob)
{
  (*hmm)->trans[from][to] = log_prob;
}

void
set_prob_emis(model_t** hmm, model_n_t state, model_n_t obs, double log_prob)
{
  (*hmm)->emis[state][obs] = log_prob;
}

model_n_t*
get_emis_tied_all(model_t* hmm)
{
  return hmm->emis_tied;
}

double*
get_prob_start_all(model_t* hmm)
{
  return hmm->start;
}

double*
get_prob_trans_all(model_t* hmm, model_n_t from)
{
  return hmm->trans[from];
}

double*
get_prob_emis_all(model_t* hmm, model_n_t state)
{
  return hmm->emis[state];
}
