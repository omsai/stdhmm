/* Alphabetize, unless there's a compelling reason. */
#include <math.h>		/* INFINITY, exp, pow, sqrt */

#include "fwd_bkw-internal.h"
#include "log_sum_exp-internal.h"
#include "model.h"

/* Forward algorithm */

void
forward_fill_matrix(matrix_t* matrix, int* obs, model_t* hmm, size_t len)
{
  /* First column is product of the starting probability and emissions. */
  for (model_n_t state = 0; state < get_n_states(hmm); ++state) {
    matrix->cells[state][0] =
      get_prob_start(hmm, state) +
      get_prob_emis(hmm, state, obs[0]);
  }

  /* Propagate remaining columns. */
  for (size_t i = 1; i < len; ++i) {
    /* Sum the product of transition edges with the previous column,
     * then multiply with emission. */
    for (model_n_t state = 0; state < get_n_states(hmm); ++state) {
      double log_sum = 0.0 - INFINITY;
      for (model_n_t state_prev = 0; state_prev < get_n_states(hmm);
	   ++state_prev) {
	log_sum =
	  log_sum_exp(log_sum,
		      matrix->cells[state_prev][i-1] +
		      get_prob_trans(hmm, state_prev, state));
      }
      matrix->cells[state][i] = log_sum + get_prob_emis(hmm, state, obs[i]);
    }
  }
}

/* Backward algorithm */

void
backward_fill_matrix(matrix_t* matrix, int* obs, model_t* hmm, size_t len)
{
  /* Last column is log(1). */
  for (model_n_t state = 0; state < get_n_states(hmm); ++state) {
    matrix->cells[state][len-1] = 0;
  }

  /* Propagate remaining columns. */
  for (int i = len - 2; i >= 0; --i) {
    /* Sum the product of transition edges with the previous column,
     * then multiply with emission. */
    for (model_n_t state = 0; state < get_n_states(hmm); ++state) {
      double log_sum = 0.0 - INFINITY;
      for (model_n_t state_next = 0; state_next < get_n_states(hmm);
	   ++state_next) {
	log_sum =
	  log_sum_exp(log_sum,
		      get_prob_trans(hmm, state, state_next) +
		      get_prob_emis(hmm, state_next, obs[i+1]) +
		      matrix->cells[state_next][i+1]);
      }
      matrix->cells[state][i] = log_sum;
    }
  }
  /* Don't calculate the -1 column because it's not used in any Baum-Welch
   * updates for emissions or transitions.  A unit test separately calculates
   * the -1 column to confirm that the forward and backward total probabilities
   * are equal. */
}

/* Baum-Welch algorithm */

void
calculate_emissions(size_t states, size_t obss, double emis[states][obss],
		    matrix_t* fwd, matrix_t* bkw, int* obs, size_t len)
{
  /* Sum probabilities by observation state.  Because we're adding
   * probabilities and not multiplying, use log_sum_exp. */
  double row_sums[states];
  /* Initialize values to zero. */
  for (model_n_t state = 0; state < states; ++state) {
    row_sums[state] = 0.0 - INFINITY;
    for (model_n_t ob = 0; ob < obss; ++ob) {
      emis[state][ob] = 0.0 - INFINITY;
    }
  }
  /* Sum probabilities along observations. */
  for (size_t i = 0; i < len; ++i) {
    for (model_n_t state = 0; state < states; ++state) {
      /* Multiply forward and backward matrices, and sum into emission
       * matrix. */
      emis[state][obs[i]] =
	log_sum_exp(emis[state][obs[i]],
		    fwd->cells[state][i] +
		    bkw->cells[state][i]);
    }
  }
  /* Row sums will be used to normalize the emission matrix. */
  for (model_n_t state = 0; state < states; ++state) {
    for (model_n_t ob = 0; ob < obss; ++ob) {
      row_sums[state] = log_sum_exp(row_sums[state], emis[state][ob]);
    }
  }
  /* Normalize the emissions. */
  for (model_n_t state = 0; state < states; ++state) {
    for (model_n_t ob = 0; ob < obss; ++ob) {
      /* emis / row_sums. */
      emis[state][ob] = emis[state][ob] - row_sums[state];
    }
  }
}

void
update_emissions(model_t* hmm, int states, int obss, double emis[states][obss])
{
  for (model_n_t state = 0; state < states; ++state) {
    for (model_n_t ob = 0; ob < obss; ++ob) {
      set_prob_emis(&hmm, state, ob, emis[state][ob]);
    }
  }
}

void
calculate_transitions(size_t states, double trans[states][states],
		      matrix_t* fwd, matrix_t* bkw, int* obs, model_t* hmm,
		      size_t len)
{
  /* Sum probabilities by transition state.  Because we're adding probabilities
   * and not multiplying, use log_sum_exp. */
  double row_sums[states];
  /* Initialize values to zero. */
  for (model_n_t from = 0; from < states; ++from) {
    row_sums[from] = 0.0 - INFINITY;
    for (model_n_t to = 0; to < states; ++to) {
      trans[from][to] = 0.0 - INFINITY;
    }
  }
  /* Sum observation transitions. */
  for (size_t i = 0; i < len - 1; ++i) {
    for (model_n_t from = 0; from < states; ++from) {
      for (model_n_t to = 0; to < states; ++to) {
	/* Multiply forward i into backward i+1 matrices via transition and
	 * emission.  Note that results are transposed by storing them as
	 * trans[to][from] instead of trans[from][to]. */
	trans[to][from] =
	  log_sum_exp(trans[to][from],
		      fwd->cells[from][i] +
		      get_prob_trans(hmm, from, to) +
		      get_prob_emis(hmm, to, obs[i+1]) +
		      bkw->cells[to][i+1]);
      }
    }
  }
  /* Row sums will be used to normalize the transition matrix. */
  for (model_n_t from = 0; from < states; ++from) {
    for (model_n_t to = 0; to < states; ++to) {
      row_sums[from] = log_sum_exp(row_sums[from], trans[from][to]);
    }
  }
  /* Normalize the transitions. */
  for (model_n_t from = 0; from < states; ++from) {
    for (model_n_t to = 0; to < states; ++to) {
      /* trans / row_sums. */
      trans[from][to] = trans[from][to] - row_sums[from];
    }
  }
}

void
update_transitions(model_t* hmm, int states, double trans[states][states])
{
  for (model_n_t from = 0; from < states; ++from) {
    for (model_n_t to = 0; to < states; ++to) {
      set_prob_trans(&hmm, from, to, trans[from][to]);
    }
  }
}

double
l2_norm_delta(size_t m, size_t n, double mat[m][n], model_t* hmm,
	      double (*accessor)(model_t*, model_n_t, model_n_t))
{
  double sum_squares = 0.0;
  for (size_t i = 0; i < m; ++i)
    for (size_t j = 0; j < n; ++j)
      sum_squares += pow(exp(mat[i][j]) -
			 exp(accessor(hmm, i, j)), 2);
  return sqrt(sum_squares);
}

double
baum_welch_single(matrix_t* fwd, matrix_t* bkw, int* obs, model_t* hmm,
		  size_t len)
{
  forward_fill_matrix(fwd, obs, hmm, len);
  backward_fill_matrix(bkw, obs, hmm, len);

  size_t states = get_n_states(hmm);
  size_t obss = get_n_obs(hmm);
  double emis[states][obss];
  double trans[states][states];
  calculate_emissions(states, obss, emis, fwd, bkw, obs, len);
  calculate_transitions(states, trans, fwd, bkw, obs, hmm, len);

  double l2_norm =
    l2_norm_delta(states, obss, emis, hmm, get_prob_emis) +
    l2_norm_delta(states, states, trans, hmm, get_prob_trans);

  update_emissions(hmm, states, obss, emis);
  update_transitions(hmm, states, trans);

  return l2_norm;
}
