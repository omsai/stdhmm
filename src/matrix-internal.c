#include <stdlib.h>		/* malloc, free, exit */

#include "matrix-internal.h"	/* matrix_t */
#include "ptr-internal.h"	/* check_ptr */

void
matrix_init(matrix_t** matrix, size_t n_rows, size_t len)
{
  *matrix = malloc(sizeof(matrix_t));
  check_ptr(matrix, "matrix");

  (*matrix)->len = len;
  (*matrix)->n_rows = n_rows;

  (*matrix)->cells = malloc(sizeof(double*) * len);
  check_ptr((*matrix)->cells, "cells");
  for (size_t i = 0; i < len; ++i) {
    (*matrix)->cells[i] = malloc(sizeof(double) * len);
    check_ptr((*matrix)->cells[i], "cells");
  }
}

void
matrix_destroy(matrix_t** matrix)
{
  for (size_t i = 0; i < (*matrix)->len; ++i) {
    free((*matrix)->cells[i]);
    (*matrix)->cells[i] = NULL;
  }
  free((*matrix)->cells);
  (*matrix)->cells = NULL;

  free((*matrix));
  *matrix = NULL;
}
