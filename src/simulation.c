#include "simulation.h"

void
simulate(int* obs, int* states, model_t* hmm, size_t len, prng_t* gen)
{
  /* Generate the initial hidden value and corresponding emitted
     observation. */
  states[0] =
    random_sample(&gen, get_prob_start_all(hmm), get_n_states(hmm));
  obs[0] =
    random_sample(&gen, get_prob_emis_all(hmm, states[0]), get_n_obs(hmm));
  for (size_t i = 1; i < len; ++i) {
    states[i] =
      random_sample(&gen, get_prob_trans_all(hmm, states[i-1]),
		    get_n_states(hmm));
    obs[i] =
      random_sample(&gen, get_prob_emis_all(hmm, states[i]), get_n_obs(hmm));
  }
}

