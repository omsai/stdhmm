#include <limits.h>		/* ULONG_MAX */
#include <math.h>		/* log */
#include <stdlib.h>		/* malloc */

#include "log_sum_exp-internal.h"
#include "ptr-internal.h"
#include "random.h"

/* The C standard library lacks any thread-safe implementation of a
   random number generator and we don't want to rely on the POSIX
   random number generator for cross-platform compatibility, therefore
   implement our own thread-safe random number generator. This is the
   Well Equilibridistributed Long-period Linear (WELL) algorithm,
   which is an updated algorithm of the standard C++ MT19937 algorithm
   by the one of its authors. */

/** Thread-safe, non-global state of the random number generator. */
struct prng_st {
  /** Internal state registers of the generator. */
  unsigned long state[16];
  /** Starting seed. */
  unsigned long seed;
  /** Internal lookup index. */
  unsigned int index;
};

void
random_init(prng_t** generator, unsigned long seed)
{
  *generator = malloc(sizeof(prng_t));
  check_ptr(generator, "generator");
  
  (*generator)->index = 0;
  (*generator)->seed = seed;
  (*generator)->state[0] = seed;
  for (int i = 1; i < 16; ++i) {
    (*generator)->state[i] = 0;
  }
}

/* Public domain WELL512 algorithm from:
   http://lomont.org/papers/2008/Lomont_PRNG_2008.pdf */

unsigned long
random_draw(prng_t** p)
{
  unsigned long a, b, c, d;

  a = (*p)->state[(*p)->index];
  c = (*p)->state[((*p)->index + 13) & 15];
  b = a ^ c ^ (a << 16) ^ (c << 15);
  c = (*p)->state[((*p)->index + 13) & 15];
  c ^= (c >> 11);
  a = (*p)->state[(*p)->index] = b^c;
  d = a ^ ((a << 5) & 0xDA442D24UL);
  (*p)->index = ((*p)->index + 15) & 15;
  a = (*p)->state[(*p)->index];
  (*p)->state[(*p)->index] =
    a ^ b ^ d ^ (a << 2) ^ (b << 18) ^ (c << 28);

  return (*p)->state[(*p)->index];
}

unsigned char
random_sample(prng_t** p, double* log_probs, unsigned char len)
{
  double log_prob = log((random_draw(p) * 1.0) / ULONG_MAX);
  unsigned char i = 0;
  for (double log_cumsum = 0.0 - INFINITY;
       i < len - 1 &&
	 log_prob > log_cumsum &&
	 log_prob <= log_sum_exp(log_cumsum, log_probs[i+1]);
       log_cumsum += log_sum_exp(log_cumsum, log_probs[i]),
       ++i);
  return i;
}

void
random_destroy(prng_t** p)
{
  free(*p);
  *p = NULL;
}
