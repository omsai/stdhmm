/* Alphabetize, unless there's a compelling reason. */
#include <math.h>		/* INFINITY */

#include "decoding.h"		/* viterbi */
#include "trellis-internal.h"	/* trellis_t */

/* Viterbi algorithm. */

void
viterbi_fill_trellis(trellis_t* trellis, int* obs, model_t* hmm, size_t len)
{
  /* Initialize starting values. */
  for (model_n_t state = 0; state < trellis->n_states; ++state) {
    trellis->nodes[0][state].prev = -1;
    trellis->nodes[0][state].log_prob = (get_prob_start(hmm, state) +
					 get_prob_emis(hmm, state, obs[0]));
  }

  /* Run Viterbi. */
  for (size_t i = 1; i < len; ++i) {
    for (model_n_t state_cur = 0; state_cur < get_n_states(hmm); ++state_cur) {
      /* For a given state, choose the highest transition probability from the
	 previous states.*/
      double prob_trans_max = (trellis->nodes[i-1][0].log_prob +
			       get_prob_trans(hmm, 0, state_cur));
      int prev = 0;
      for (model_n_t state_prev = 1; state_prev < get_n_states(hmm);
	   ++state_prev) {
	double prob_trans = (trellis->nodes[i-1][state_prev].log_prob +
			     get_prob_trans(hmm, state_prev, state_cur));
	if (prob_trans > prob_trans_max) {
	  prob_trans_max = prob_trans;
	  prev = state_prev;
	}
      }
      /* Add the emission probability. */
      trellis->nodes[i][state_cur].prev = prev;
      trellis->nodes[i][state_cur].log_prob = (prob_trans_max +
					       get_prob_emis(hmm, state_cur,
							     obs[i]));
    }
  }
}

void
viterbi_choose_path(int* ret, trellis_t* trellis, model_n_t n_states,
		    size_t len)
{
  /* Get the most probable state from the end. */
  double prob_max = 0.0 - INFINITY;
  int prev = -1;
  for (model_n_t state = 0; state < n_states; ++state)
    if (trellis->nodes[len-1][state].log_prob > prob_max) {
      prob_max = trellis->nodes[len-1][state].log_prob;
      prev = state;
    }
  ret[len-1] = prev;
  /* Backtrack for most probable states. */
  for (int i = len - 2; i >= 0; --i) {
    ret[i] = trellis->nodes[i+1][prev].prev;
    prev = trellis->nodes[i+1][prev].prev;
  }
}

void
viterbi(int* ret, int* obs, model_t* hmm, size_t len)
{
  trellis_t* trellis = NULL;
  trellis_init(&trellis, get_n_states(hmm), len);
  
  viterbi_fill_trellis(trellis, obs, hmm, len);
  viterbi_choose_path(ret, trellis, get_n_states(hmm), len);

  trellis_destroy(&trellis);
}
